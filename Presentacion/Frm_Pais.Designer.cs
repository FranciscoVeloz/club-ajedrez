﻿
namespace Presentacion
{
    partial class Frm_Pais
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.txtrepresentaPais = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscarPais = new System.Windows.Forms.TextBox();
            this.btnEliminarPais = new System.Windows.Forms.Button();
            this.btnCancelarPais = new System.Windows.Forms.Button();
            this.btnGuardarPais = new System.Windows.Forms.Button();
            this.btnNuevoPais = new System.Windows.Forms.Button();
            this.dtg_Pais = new System.Windows.Forms.DataGridView();
            this.txtNumClubsPais = new System.Windows.Forms.TextBox();
            this.txtNombrePais = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_Pais)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(875, 122);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 18);
            this.label4.TabIndex = 73;
            this.label4.Text = "Representa:";
            // 
            // txtrepresentaPais
            // 
            this.txtrepresentaPais.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrepresentaPais.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrepresentaPais.Location = new System.Drawing.Point(780, 161);
            this.txtrepresentaPais.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtrepresentaPais.Name = "txtrepresentaPais";
            this.txtrepresentaPais.Size = new System.Drawing.Size(311, 26);
            this.txtrepresentaPais.TabIndex = 72;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(477, 122);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 18);
            this.label3.TabIndex = 71;
            this.label3.Text = "Numero de Club:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(83, 122);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 18);
            this.label2.TabIndex = 70;
            this.label2.Text = "Nombre del Pais:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(521, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 18);
            this.label1.TabIndex = 69;
            this.label1.Text = "Buscar";
            // 
            // txtBuscarPais
            // 
            this.txtBuscarPais.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBuscarPais.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscarPais.Location = new System.Drawing.Point(7, 54);
            this.txtBuscarPais.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtBuscarPais.Name = "txtBuscarPais";
            this.txtBuscarPais.Size = new System.Drawing.Size(1085, 26);
            this.txtBuscarPais.TabIndex = 68;
            this.txtBuscarPais.TextChanged += new System.EventHandler(this.txtBuscarPais_TextChanged);
            // 
            // btnEliminarPais
            // 
            this.btnEliminarPais.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarPais.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarPais.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarPais.Location = new System.Drawing.Point(959, 443);
            this.btnEliminarPais.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnEliminarPais.Name = "btnEliminarPais";
            this.btnEliminarPais.Size = new System.Drawing.Size(133, 62);
            this.btnEliminarPais.TabIndex = 67;
            this.btnEliminarPais.Text = "Eliminar";
            this.btnEliminarPais.UseVisualStyleBackColor = true;
            this.btnEliminarPais.Click += new System.EventHandler(this.btnEliminarPais_Click);
            // 
            // btnCancelarPais
            // 
            this.btnCancelarPais.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarPais.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelarPais.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelarPais.Location = new System.Drawing.Point(809, 443);
            this.btnCancelarPais.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnCancelarPais.Name = "btnCancelarPais";
            this.btnCancelarPais.Size = new System.Drawing.Size(133, 62);
            this.btnCancelarPais.TabIndex = 66;
            this.btnCancelarPais.Text = "Cancelar";
            this.btnCancelarPais.UseVisualStyleBackColor = true;
            this.btnCancelarPais.Click += new System.EventHandler(this.btnCancelarPais_Click);
            // 
            // btnGuardarPais
            // 
            this.btnGuardarPais.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarPais.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarPais.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarPais.Location = new System.Drawing.Point(660, 443);
            this.btnGuardarPais.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnGuardarPais.Name = "btnGuardarPais";
            this.btnGuardarPais.Size = new System.Drawing.Size(133, 62);
            this.btnGuardarPais.TabIndex = 65;
            this.btnGuardarPais.Text = "Guardar";
            this.btnGuardarPais.UseVisualStyleBackColor = true;
            this.btnGuardarPais.Click += new System.EventHandler(this.btnGuardarPais_Click);
            // 
            // btnNuevoPais
            // 
            this.btnNuevoPais.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevoPais.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevoPais.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNuevoPais.Location = new System.Drawing.Point(511, 443);
            this.btnNuevoPais.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnNuevoPais.Name = "btnNuevoPais";
            this.btnNuevoPais.Size = new System.Drawing.Size(133, 62);
            this.btnNuevoPais.TabIndex = 64;
            this.btnNuevoPais.Text = "Nuevo";
            this.btnNuevoPais.UseVisualStyleBackColor = true;
            this.btnNuevoPais.Click += new System.EventHandler(this.btnNuevoPais_Click);
            // 
            // dtg_Pais
            // 
            this.dtg_Pais.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg_Pais.Location = new System.Drawing.Point(7, 229);
            this.dtg_Pais.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.dtg_Pais.Name = "dtg_Pais";
            this.dtg_Pais.Size = new System.Drawing.Size(1085, 182);
            this.dtg_Pais.TabIndex = 63;
            this.dtg_Pais.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_Pais_CellContentClick);
            // 
            // txtNumClubsPais
            // 
            this.txtNumClubsPais.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumClubsPais.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumClubsPais.Location = new System.Drawing.Point(393, 161);
            this.txtNumClubsPais.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtNumClubsPais.Name = "txtNumClubsPais";
            this.txtNumClubsPais.Size = new System.Drawing.Size(323, 26);
            this.txtNumClubsPais.TabIndex = 62;
            // 
            // txtNombrePais
            // 
            this.txtNombrePais.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNombrePais.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombrePais.Location = new System.Drawing.Point(7, 161);
            this.txtNombrePais.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtNombrePais.Name = "txtNombrePais";
            this.txtNombrePais.Size = new System.Drawing.Size(329, 26);
            this.txtNombrePais.TabIndex = 61;
            // 
            // Frm_Pais
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1111, 522);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtrepresentaPais);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBuscarPais);
            this.Controls.Add(this.btnEliminarPais);
            this.Controls.Add(this.btnCancelarPais);
            this.Controls.Add(this.btnGuardarPais);
            this.Controls.Add(this.btnNuevoPais);
            this.Controls.Add(this.dtg_Pais);
            this.Controls.Add(this.txtNumClubsPais);
            this.Controls.Add(this.txtNombrePais);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Pais";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pais";
            this.Load += new System.EventHandler(this.Frm_Pais_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtg_Pais)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtrepresentaPais;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscarPais;
        private System.Windows.Forms.Button btnEliminarPais;
        private System.Windows.Forms.Button btnCancelarPais;
        private System.Windows.Forms.Button btnGuardarPais;
        private System.Windows.Forms.Button btnNuevoPais;
        private System.Windows.Forms.DataGridView dtg_Pais;
        private System.Windows.Forms.TextBox txtNumClubsPais;
        private System.Windows.Forms.TextBox txtNombrePais;
    }
}