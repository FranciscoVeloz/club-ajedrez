﻿namespace Presentacion
{
    partial class Frm_Juego
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtidjuego = new System.Windows.Forms.TextBox();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.DtgJuego = new System.Windows.Forms.DataGridView();
            this.txtfkjugador = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtcolor = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtfkidpartida = new System.Windows.Forms.TextBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DtgJuego)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(495, 1);
            this.label1.Margin = new System.Windows.Forms.Padding(13, 0, 13, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 20);
            this.label1.TabIndex = 69;
            this.label1.Text = "IdJuego";
            // 
            // txtidjuego
            // 
            this.txtidjuego.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtidjuego.Location = new System.Drawing.Point(22, 41);
            this.txtidjuego.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.txtidjuego.Name = "txtidjuego";
            this.txtidjuego.Size = new System.Drawing.Size(1059, 22);
            this.txtidjuego.TabIndex = 57;
            this.txtidjuego.TextChanged += new System.EventHandler(this.txtidjuego_TextChanged);
            // 
            // btnEliminar
            // 
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(939, 505);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(139, 48);
            this.btnEliminar.TabIndex = 64;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click_1);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(768, 505);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(144, 48);
            this.btnCancelar.TabIndex = 63;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(568, 510);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(173, 43);
            this.btnGuardar.TabIndex = 62;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNuevo.Location = new System.Drawing.Point(363, 512);
            this.btnNuevo.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(163, 38);
            this.btnNuevo.TabIndex = 61;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // DtgJuego
            // 
            this.DtgJuego.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgJuego.Location = new System.Drawing.Point(-24, 214);
            this.DtgJuego.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.DtgJuego.Name = "DtgJuego";
            this.DtgJuego.Size = new System.Drawing.Size(1115, 273);
            this.DtgJuego.TabIndex = 68;
            this.DtgJuego.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgJuego_CellContentClick);
            // 
            // txtfkjugador
            // 
            this.txtfkjugador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfkjugador.Location = new System.Drawing.Point(345, 170);
            this.txtfkjugador.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.txtfkjugador.Name = "txtfkjugador";
            this.txtfkjugador.Size = new System.Drawing.Size(321, 22);
            this.txtfkjugador.TabIndex = 59;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(471, 135);
            this.label3.Margin = new System.Windows.Forms.Padding(13, 0, 13, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 20);
            this.label3.TabIndex = 65;
            this.label3.Text = "fkjugador";
            // 
            // txtcolor
            // 
            this.txtcolor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcolor.Location = new System.Drawing.Point(693, 170);
            this.txtcolor.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.txtcolor.Name = "txtcolor";
            this.txtcolor.Size = new System.Drawing.Size(383, 22);
            this.txtcolor.TabIndex = 60;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(827, 135);
            this.label2.Margin = new System.Windows.Forms.Padding(13, 0, 13, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 20);
            this.label2.TabIndex = 66;
            this.label2.Text = "color";
            // 
            // txtfkidpartida
            // 
            this.txtfkidpartida.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfkidpartida.Location = new System.Drawing.Point(11, 170);
            this.txtfkidpartida.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.txtfkidpartida.Name = "txtfkidpartida";
            this.txtfkidpartida.Size = new System.Drawing.Size(307, 22);
            this.txtfkidpartida.TabIndex = 58;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.Location = new System.Drawing.Point(125, 135);
            this.lblUsuario.Margin = new System.Windows.Forms.Padding(13, 0, 13, 0);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(109, 20);
            this.lblUsuario.TabIndex = 67;
            this.lblUsuario.Text = "Fk_idpartida";
            // 
            // Frm_Juego
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 562);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtidjuego);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.DtgJuego);
            this.Controls.Add(this.txtfkjugador);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtcolor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtfkidpartida);
            this.Controls.Add(this.lblUsuario);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Juego";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Juego";
            this.Load += new System.EventHandler(this.Frm_Juego_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DtgJuego)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidjuego;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.DataGridView DtgJuego;
        private System.Windows.Forms.TextBox txtfkjugador;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtcolor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtfkidpartida;
        private System.Windows.Forms.Label lblUsuario;
    }
}