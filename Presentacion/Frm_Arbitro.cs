﻿using System;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_Arbitro : Form
    {
        private ManejadorArbitro _manejador;
        private EntidadArbitro _entidad;
        private string banderaGuardar;

        public Frm_Arbitro()
        {
            InitializeComponent();
            _manejador = new ManejadorArbitro();
            _entidad = new EntidadArbitro();
        }

        //Metodo para botonera
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        //Metodo para los textbox
        private void ControlTxt(Boolean nombre, Boolean direccion, Boolean pais)
        {
            txtNombre.Enabled = nombre;
            txtDireccion.Enabled = direccion;
            txtPais.Enabled = pais;
        }

        //Metodo para limpiar los textbox
        private void LimpiarTxt()
        {
            txtNombre.Clear();
            txtDireccion.Clear();
            txtPais.Clear();
        }

        //Metodo actualizar datagridview
        private void BuscarArbitro(string consulta)
        {
            DtgArbitro.DataSource = _manejador.Mostrar(consulta);
            DtgArbitro.AutoResizeColumns();
        }

        //Metodo guardar
        private void GuardarArbitro()
        {
            try
            {
                EntidadArbitro entidad = new EntidadArbitro();
                entidad.Nombre = txtNombre.Text;
                entidad.Direccion = txtDireccion.Text;
                entidad.FkPais = int.Parse(txtPais.Text);

                var validar = _manejador.ValidarCampo(entidad);

                if (validar.Item1)
                {
                    _manejador.Insertar(entidad);
                    MessageBox.Show("El usuario se guardo correctamente");
                }

                else
                {
                    MessageBox.Show(validar.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("El campo país no puede ser vacio", "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Metodo actualizar
        private void Actualizar()
        {
            _manejador.Actualizar(new EntidadArbitro
            {
                IdArbitro = int.Parse(DtgArbitro.CurrentRow.Cells["idArbitro"].Value.ToString()),
                Nombre = txtNombre.Text,
                Direccion = txtDireccion.Text,
                FkPais = int.Parse(txtPais.Text)
            });
        }

        //Metodo eliminar
        private void Eliminar()
        {
            var idArbitro = DtgArbitro.CurrentRow.Cells["idArbitro"].Value.ToString();
            _manejador.Eliminar(int.Parse(idArbitro));
        }

        //Evento load del form
        private void Frm_Arbitro_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false);
            BuscarArbitro(txtBuscar.Text);
        }

        //Evento click boton nuevo
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            ControlTxt(true, true, true);
            txtNombre.Focus();
            banderaGuardar = "guardar";
        }

        //Evento click boton guardar
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {
                GuardarArbitro();
                BuscarArbitro("");
            }

            else
            {
                Actualizar();
                BuscarArbitro("");
            }

            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false);
        }

        //Evento click boton cancelar
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false);
        }

        //Evento cambio de texto buscar
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarArbitro(txtBuscar.Text);
        }

        //Evento click datagridview
        private void DtgArbitro_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, true);
            ControlTxt(true, true, true);
            txtNombre.Focus();
            txtNombre.Text = DtgArbitro.CurrentRow.Cells["nombre"].Value.ToString();
            txtDireccion.Text = DtgArbitro.CurrentRow.Cells["direccion"].Value.ToString();
            txtPais.Text = DtgArbitro.CurrentRow.Cells["fkPais"].Value.ToString();
            banderaGuardar = "actualizar";
        }

        //Evento click boton eliminar
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro que quieres eliminar este dato?", "Eliminar Arbitro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarArbitro("");
                MessageBox.Show("Usuario eliminado");
            }
        }
    }
}