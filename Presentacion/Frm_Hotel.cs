﻿using Entidades;
using Manejadores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Frm_Hotel : Form
    {
        private ManejadorHotel _manejadorhotel;
        private EntidadHotel _entidadhotel;
        private string banderaGuardarHotel;
        public Frm_Hotel()
        {
            InitializeComponent();
            _manejadorhotel = new ManejadorHotel();
            _entidadhotel = new EntidadHotel();
        }
        //Metodo para botonera
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevoHotel.Enabled = nuevo;
            btnGuardarHotel.Enabled = guardar;
            btnCancelarHotel.Enabled = cancelar;
            btnEliminarHotel.Enabled = eliminar;
        }

        //Metodo para los textbox
        private void TextBox(Boolean nombrehotel, Boolean direccionhotel, Boolean telefonohotel)
        {
            txtNombreHotel.Enabled = nombrehotel;
            txtDireccionHotel.Enabled = direccionhotel;
            txtTelefonoHotel.Enabled = telefonohotel;
        }

        //Metodo para limpiar los textbox
        private void LimpiarTxt()
        {
            txtNombreHotel.Clear();
            txtDireccionHotel.Clear();
            txtTelefonoHotel.Clear();
        }
        //Metodo actualizar datagridview
        private void BuscarHotel(string consulta)
        {
            dtg_Hotel.DataSource = _manejadorhotel.Mostrar(consulta);
        }

        //Metodo guardar
        private void GuardarHoteles()
        {
            EntidadHotel entidadhotel = new EntidadHotel();
            entidadhotel.NombreHotel = txtNombreHotel.Text;
            entidadhotel.DireccionHotel = txtDireccionHotel.Text;
            entidadhotel.TelefonoHotel = txtTelefonoHotel.Text;

            var validar = _manejadorhotel.ValidarHotel(entidadhotel);

            if (validar.Item1)
            {
                _manejadorhotel.HotelInsertar(entidadhotel);
                MessageBox.Show("El usuario se guardo correctamente");
            }

            else
            {
                MessageBox.Show(validar.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Metodo actualizar
        private void ActualizarHoteles()
        {
            _manejadorhotel.HotelActualizar(new EntidadHotel
            {
                IdHotel = int.Parse(dtg_Hotel.CurrentRow.Cells["idHotel"].Value.ToString()),
                NombreHotel = txtNombreHotel.Text,
                DireccionHotel = txtDireccionHotel.Text,
                TelefonoHotel = txtTelefonoHotel.Text
            });
        }

        //Metodo eliminar
        private void EliminarHoteles()
        {
            var idhotel = dtg_Hotel.CurrentRow.Cells["IdHotel"].Value.ToString();
            _manejadorhotel.HotelEliminar(int.Parse(idhotel));
        }
        private void btnNuevoHotel_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            TextBox(true, true, true);
            txtNombreHotel.Focus();
            banderaGuardarHotel = "guardar";
        }

        private void Frm_Hotel_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarTxt();
            TextBox(false, false, false);
            BuscarHotel("");
        }

        private void btnGuardarHotel_Click(object sender, EventArgs e)
        {
            if (banderaGuardarHotel == "guardar")
            {
                GuardarHoteles();
                BuscarHotel("");
            }
            else
            {
                ActualizarHoteles();
                BuscarHotel("");
            }
            Botonera(true, false, false, true);
            LimpiarTxt();
            TextBox(false, false, false);
        }

        private void btnEliminarHotel_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Quieres eliminar a el Hotel: { _entidadhotel.NombreHotel}?", "Eliminar Hotel", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                EliminarHoteles();
                BuscarHotel("");
                MessageBox.Show("Hotel eliminado");
                Botonera(true, false, false, true);
                LimpiarTxt();
                TextBox(false, false, false);
            }
        }

        private void btnCancelarHotel_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarTxt();
            TextBox(false, false, false);
        }

        private void txtBuscarHotel_TextChanged(object sender, EventArgs e)
        {
            BuscarHotel(txtBuscarHotel.Text);
        }

        private void dtg_Hotel_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, true);
            TextBox(true, true, true);
            txtNombreHotel.Focus();
            txtNombreHotel.Text = dtg_Hotel.CurrentRow.Cells["NombreHotel"].Value.ToString();
            txtDireccionHotel.Text = dtg_Hotel.CurrentRow.Cells["DireccionHotel"].Value.ToString();
            txtTelefonoHotel.Text = dtg_Hotel.CurrentRow.Cells["TelefonoHotel"].Value.ToString();
            banderaGuardarHotel = "actualizar";
        }
    }
}
