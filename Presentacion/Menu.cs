﻿using System;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ejemploToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void paísToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Pais pais = new Frm_Pais();
            pais.ShowDialog();
        }

        private void arbitroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Arbitro arbitro = new Frm_Arbitro();
            arbitro.ShowDialog();
        }

        private void jugadorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Jugador jugador = new Frm_Jugador();
            jugador.ShowDialog();
        }

        private void campeonatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Campeonatos campeonatos = new Frm_Campeonatos();
            campeonatos.ShowDialog();
        }

        private void hotelesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Hotel hotel = new Frm_Hotel();
            hotel.ShowDialog();
        }

        private void salasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Sala sala = new Frm_Sala();
            sala.ShowDialog();
        }

        private void reservasToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Frm_ReservaJugador jugador = new Frm_ReservaJugador();
            jugador.ShowDialog();
        }

        private void partidasToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Frm_Partida partida = new Frm_Partida();
            partida.ShowDialog();
        }

        private void juegosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Juego juego = new Frm_Juego();
            juego.ShowDialog();
        }

        private void movimientosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Movimientos movimientos = new Frm_Movimientos();
            movimientos.ShowDialog();
        }

        private void reservaArbitroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_ReservaArbitro arbitro = new Frm_ReservaArbitro();
            arbitro.ShowDialog();
        }
    }
}
