﻿namespace Presentacion
{
    partial class Frm_Sala
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtidsala = new System.Windows.Forms.TextBox();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.DtgSala = new System.Windows.Forms.DataGridView();
            this.txtcapacidad = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtmedios = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtfkidhotel = new System.Windows.Forms.TextBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DtgSala)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(416, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 56;
            this.label1.Text = "IdSala";
            // 
            // txtidsala
            // 
            this.txtidsala.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtidsala.Location = new System.Drawing.Point(19, 32);
            this.txtidsala.Margin = new System.Windows.Forms.Padding(10, 7, 10, 7);
            this.txtidsala.Name = "txtidsala";
            this.txtidsala.Size = new System.Drawing.Size(846, 22);
            this.txtidsala.TabIndex = 44;
            this.txtidsala.TextChanged += new System.EventHandler(this.txtidsala_TextChanged);
            // 
            // btnEliminar
            // 
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(746, 418);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(10, 7, 10, 7);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(104, 39);
            this.btnEliminar.TabIndex = 51;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(618, 418);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(10, 7, 10, 7);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 39);
            this.btnCancelar.TabIndex = 50;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(468, 418);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(10, 7, 10, 7);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(130, 39);
            this.btnGuardar.TabIndex = 49;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNuevo.Location = new System.Drawing.Point(314, 418);
            this.btnNuevo.Margin = new System.Windows.Forms.Padding(10, 7, 10, 7);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(122, 37);
            this.btnNuevo.TabIndex = 48;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // DtgSala
            // 
            this.DtgSala.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgSala.Location = new System.Drawing.Point(19, 182);
            this.DtgSala.Margin = new System.Windows.Forms.Padding(10, 7, 10, 7);
            this.DtgSala.Name = "DtgSala";
            this.DtgSala.Size = new System.Drawing.Size(846, 222);
            this.DtgSala.TabIndex = 55;
            this.DtgSala.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgSala_CellContentClick);
            // 
            // txtcapacidad
            // 
            this.txtcapacidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcapacidad.Location = new System.Drawing.Point(322, 118);
            this.txtcapacidad.Margin = new System.Windows.Forms.Padding(10, 7, 10, 7);
            this.txtcapacidad.Name = "txtcapacidad";
            this.txtcapacidad.Size = new System.Drawing.Size(241, 22);
            this.txtcapacidad.TabIndex = 46;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(400, 95);
            this.label3.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 16);
            this.label3.TabIndex = 52;
            this.label3.Text = "Capacidad";
            // 
            // txtmedios
            // 
            this.txtmedios.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmedios.Location = new System.Drawing.Point(634, 118);
            this.txtmedios.Margin = new System.Windows.Forms.Padding(10, 7, 10, 7);
            this.txtmedios.Name = "txtmedios";
            this.txtmedios.Size = new System.Drawing.Size(231, 22);
            this.txtmedios.TabIndex = 47;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(720, 95);
            this.label2.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 16);
            this.label2.TabIndex = 53;
            this.label2.Text = "Medios";
            // 
            // txtfkidhotel
            // 
            this.txtfkidhotel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfkidhotel.Location = new System.Drawing.Point(19, 118);
            this.txtfkidhotel.Margin = new System.Windows.Forms.Padding(10, 7, 10, 7);
            this.txtfkidhotel.Name = "txtfkidhotel";
            this.txtfkidhotel.Size = new System.Drawing.Size(231, 22);
            this.txtfkidhotel.TabIndex = 45;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(93, 90);
            this.lblUsuario.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(83, 16);
            this.lblUsuario.TabIndex = 54;
            this.lblUsuario.Text = "Fk_idHotel";
            // 
            // Frm_Sala
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtidsala);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.DtgSala);
            this.Controls.Add(this.txtcapacidad);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtmedios);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtfkidhotel);
            this.Controls.Add(this.lblUsuario);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Sala";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Sala";
            this.Load += new System.EventHandler(this.Frm_Sala_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DtgSala)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidsala;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.DataGridView DtgSala;
        private System.Windows.Forms.TextBox txtcapacidad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtmedios;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtfkidhotel;
        private System.Windows.Forms.Label lblUsuario;
    }
}