﻿using System;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_Jugador : Form
    {
        private ManejadorJugador _manejador;
        private EntidadJugador _entidad;
        private string banderaGuardar;

        public Frm_Jugador()
        {
            InitializeComponent();
            _manejador = new ManejadorJugador();
            _entidad = new EntidadJugador();
        }

        //Metodo para botonera
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        //Metodo para los textbox
        private void ControlTxt(Boolean nombre, Boolean direccion, Boolean pais, Boolean nivel)
        {
            txtNombre.Enabled = nombre;
            txtDireccion.Enabled = direccion;
            txtPais.Enabled = pais;
            txtNivel.Enabled = nivel;
        }

        //Metodo para limpiar los textbox
        private void LimpiarTxt()
        {
            txtNombre.Clear();
            txtDireccion.Clear();
            txtPais.Clear();
            txtNivel.Clear();
        }

        //Metodo actualizar datagridview
        private void BuscarJugador(string consulta)
        {
            DtgJugador.DataSource = _manejador.Mostrar(consulta);
            DtgJugador.AutoResizeColumns();
        }

        //Metodo guardar
        private void GuardarJugador()
        {
            try
            {
                EntidadJugador entidad = new EntidadJugador();
                entidad.Nombre = txtNombre.Text;
                entidad.Direccion = txtDireccion.Text;
                entidad.FkPais = int.Parse(txtPais.Text);
                entidad.Nivel = int.Parse(txtNivel.Text);

                var validar = _manejador.ValidarCampo(entidad);

                if (validar.Item1)
                {
                    _manejador.Insertar(entidad);
                    MessageBox.Show("El usuario se guardo correctamente");
                }

                else
                {
                    MessageBox.Show(validar.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("El campo país/nivel no puede ser vacio", "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Metodo actualizar
        private void Actualizar()
        {
            _manejador.Actualizar(new EntidadJugador
            {
                IdJugador = int.Parse(DtgJugador.CurrentRow.Cells["idJugador"].Value.ToString()),
                Nombre = txtNombre.Text,
                Direccion = txtDireccion.Text,
                FkPais = int.Parse(txtPais.Text),
                Nivel = int.Parse(txtNivel.Text)
            });
        }

        //Metodo eliminar
        private void Eliminar()
        {
            var idJugador = DtgJugador.CurrentRow.Cells["idJugador"].Value.ToString();
            _manejador.Eliminar(int.Parse(idJugador));
        }

        //Evento load del form
        private void Frm_Jugador_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
            BuscarJugador("");
        }

        //Evento click boton nuevo
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            ControlTxt(true, true, true, true);
            txtNombre.Focus();
            banderaGuardar = "guardar";
        }

        //Evento click boton guardar
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {
                GuardarJugador();
                BuscarJugador("");
            }

            else
            {
                Actualizar();
                BuscarJugador("");
            }

            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
        }

        //Evento click boton cancelar
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
        }

        //Evento cambio de texto buscar
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarJugador(txtBuscar.Text);
        }

        //Evento click datagridview
        private void DtgJugador_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, true);
            ControlTxt(true, true, true, true);
            txtNombre.Focus();
            txtNombre.Text = DtgJugador.CurrentRow.Cells["nombre"].Value.ToString();
            txtDireccion.Text = DtgJugador.CurrentRow.Cells["direccion"].Value.ToString();
            txtPais.Text = DtgJugador.CurrentRow.Cells["fkPais"].Value.ToString();
            txtNivel.Text = DtgJugador.CurrentRow.Cells["nivel"].Value.ToString();
            banderaGuardar = "actualizar";
        }

        //Evento click boton eliminar
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro que quieres eliminar este dato?", "Eliminar Jugador", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Botonera(true, false, false, false);
                LimpiarTxt();
                ControlTxt(false, false, false, false);
                BuscarJugador("");
                MessageBox.Show("Usuario eliminado");
            }
        }
    }
}