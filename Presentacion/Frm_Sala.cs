﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_Sala : Form
    {
        private ManejadorSala _manejador;
        private EntidadSala _entidad;
        private string banderaGuardar;
        public Frm_Sala()
        {
            InitializeComponent();
            _manejador = new ManejadorSala();
            _entidad = new EntidadSala();
        }
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }
        //Metodo para los textbox
        private void ControlTxt(Boolean idsala, Boolean fkhotel, Boolean capacidad, Boolean medios)
        {
            txtidsala.Enabled = idsala;
            txtfkidhotel.Enabled = fkhotel;
            txtcapacidad.Enabled = capacidad;
            txtmedios.Enabled = medios;
        }
        //Metodo para limpiar los textbox
        private void LimpiarTxt()
        {
            txtidsala.Clear();
            txtcapacidad.Clear();
            txtfkidhotel.Clear();
            txtmedios.Clear();
        }
        private void BuscarSala(string consulta)
        {
            DtgSala.DataSource = _manejador.Mostrar(consulta);
        }
        //Metodo guardar
        private void GuardarSala()
        {
            EntidadSala entidad = new EntidadSala();
            entidad.Fk_idhotel = int.Parse(txtfkidhotel.Text);
            entidad.Capacidad = int.Parse(txtcapacidad.Text);
            entidad.Medios = txtmedios.Text;

            var validar = _manejador.ValidarCampo(entidad);

            if (validar.Item1)
            {
                _manejador.Insertar(entidad);
                MessageBox.Show("La sala se guardo correctamente");
            }

            else
            {
                MessageBox.Show(validar.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //Metodo actualizar
        private void Actualizar()
        {
            _manejador.Actualizar(new EntidadSala
            {
                Idsala = int.Parse(DtgSala.CurrentRow.Cells["idsala"].Value.ToString()),
                Fk_idhotel = int.Parse(txtfkidhotel.Text),
                Capacidad = int.Parse(txtcapacidad.Text),
                Medios = txtmedios.Text
            }) ;
        }
        //Metodo eliminar
        private void Eliminar()
        {
            var idsala = DtgSala.CurrentRow.Cells["IdSala"].Value.ToString();
            _manejador.Eliminar(idsala);
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Quieres eliminar a la sala{ _entidad.Idsala }?", "Eliminar Sala", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarSala("");
                MessageBox.Show("Sala eliminado");
            }
        }

        private void Frm_Sala_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
            BuscarSala(txtidsala.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            ControlTxt(true, true, true, true);
            txtidsala.Focus();
            banderaGuardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {
                GuardarSala();
                BuscarSala("");
            }

            else
            {
                Actualizar();
                BuscarSala("");
            }

            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {

            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false,false);
        }

        private void txtidsala_TextChanged(object sender, EventArgs e)
        {
            BuscarSala(txtidsala.Text);
        }

        private void DtgSala_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, true);
            ControlTxt(true, true, true, true);
            txtidsala.Focus();
            txtidsala.Text = DtgSala.CurrentRow.Cells["IdSala"].Value.ToString();
            txtfkidhotel.Text = DtgSala.CurrentRow.Cells["FK_idhotel"].Value.ToString();
            txtcapacidad.Text = DtgSala.CurrentRow.Cells["Capacidad"].Value.ToString();
            txtmedios.Text = DtgSala.CurrentRow.Cells["Medios"].Value.ToString();
            banderaGuardar = "actualizar";
        }
    }
}        