﻿using Entidades;
using Manejadores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Frm_ReservaJugador : Form
    {
        private ManejadorReservaJugador _manejadorreservaJ;
        private EntidadReservaJugador _entidadreservaJ;
        private string banderaGuardarReservaJugador;
        public Frm_ReservaJugador()
        {
            InitializeComponent();
            _manejadorreservaJ = new ManejadorReservaJugador();
            _entidadreservaJ = new EntidadReservaJugador();
        }
        //Metodo para botonera
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevoReservaJugador.Enabled = nuevo;
            btnGuardarReservaJugador.Enabled = guardar;
            btnCancelarReservaJugador.Enabled = cancelar;
            btnEliminarReservaJugador.Enabled = eliminar;
        }

        //Metodo para los textbox
        private void ControlTxt(Boolean fechaentradaj, Boolean fechasalidaj, Boolean fkidhotel, Boolean fkidjugador)
        {
            txtFechaEntradaJugdor.Enabled = fechaentradaj;
            txtFechaSalidaJugador.Enabled = fechasalidaj;
            txtFkIdHotel.Enabled = fkidhotel;
            txtFkIdJugador.Enabled = fkidjugador;
        }

        //Metodo para limpiar los textbox
        private void LimpiarTxt()
        {
            txtFechaEntradaJugdor.Clear();
            txtFechaSalidaJugador.Clear();
            txtFkIdHotel.Clear();
            txtFkIdJugador.Clear();
        }

        //Metodo actualizar datagridview
        private void BuscarReservaJugador(string consulta)
        {
            dtg_ReservaJugador.DataSource = _manejadorreservaJ.Mostrar(consulta);
        }

        //Metodo guardar
        private void GuardarReservaJugador()
        {
            EntidadReservaJugador entidadreservaJ = new EntidadReservaJugador();
            entidadreservaJ.FechaEntradaJugador = txtFechaEntradaJugdor.Text;
            entidadreservaJ.FechaSalidaJugador = txtFechaSalidaJugador.Text;
            entidadreservaJ.FkIdhotel= int.Parse(txtFkIdHotel.Text);
            entidadreservaJ.FkIdjugador = int.Parse(txtFkIdJugador.Text);

            var validar = _manejadorreservaJ.ValidarJugadorReserva(entidadreservaJ);

            if (validar.Item1)
            {
                _manejadorreservaJ.ReservaJugadorInsertar(entidadreservaJ);
                MessageBox.Show("El usuario se guardo correctamente");
            }

            else
            {
                MessageBox.Show(validar.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Metodo actualizar
        private void Actualizar()
        {
            _manejadorreservaJ.ReservaJugadorActualizar(new EntidadReservaJugador
            {
                IdReservaJugador = int.Parse(dtg_ReservaJugador.CurrentRow.Cells["idReservaJugador"].Value.ToString()),
                FechaEntradaJugador = txtFechaEntradaJugdor.Text,
                FechaSalidaJugador = txtFechaSalidaJugador.Text,
                FkIdhotel = int.Parse(txtFkIdJugador.Text),
                FkIdjugador = int.Parse(txtFkIdJugador.Text)
            });
        }

        //Metodo eliminar
        private void Eliminar()
        {
            var idreservajugador = dtg_ReservaJugador.CurrentRow.Cells["IdReservaJugador"].Value.ToString();
            _manejadorreservaJ.ReservaJugadorEliminar(int.Parse(idreservajugador));
        }
        private void btnNuevoReservaJugador_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            ControlTxt(true, true, true,true);
            txtFechaEntradaJugdor.Focus();
            banderaGuardarReservaJugador = "guardar";
        }

        private void btnGuardarReservaJugador_Click(object sender, EventArgs e)
        {
            if (banderaGuardarReservaJugador == "guardar")
            {
                GuardarReservaJugador();
                BuscarReservaJugador("");
            }

            else
            {
                Actualizar();
                BuscarReservaJugador("");
            }

            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false,false);
        }

        private void btnCancelarReservaJugador_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false,false);
        }

        private void btnEliminarReservaJugador_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Quieres eliminar a la reserva de Jugador: { _entidadreservaJ.FechaEntradaJugador }?", "Eliminar Reserva Jugador", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarReservaJugador("");
                MessageBox.Show("Reserva Jugador eliminada");
            }
        }

        private void dtg_ReservaJugador_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, true);
            ControlTxt(true, true, true,true);
            txtFechaEntradaJugdor.Focus();
            txtFechaSalidaJugador.Text = dtg_ReservaJugador.CurrentRow.Cells["FechaSalidaJugador"].Value.ToString();
            txtFkIdHotel.Text = dtg_ReservaJugador.CurrentRow.Cells["FkIdhotel"].Value.ToString();
            txtFkIdJugador.Text = dtg_ReservaJugador.CurrentRow.Cells["FkIdjugador"].Value.ToString();
            banderaGuardarReservaJugador = "actualizar";
        }

        private void Frm_ReservaJugador_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarTxt();
            ControlTxt(false, false, false,false);
            BuscarReservaJugador(txtBuscarReservaJugador.Text);
        }

        private void txtBuscarReservaJugador_TextChanged(object sender, EventArgs e)
        {
            BuscarReservaJugador(txtBuscarReservaJugador.Text);
        }
    }
}
