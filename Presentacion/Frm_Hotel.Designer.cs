﻿
namespace Presentacion
{
    partial class Frm_Hotel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.txtTelefonoHotel = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscarHotel = new System.Windows.Forms.TextBox();
            this.btnEliminarHotel = new System.Windows.Forms.Button();
            this.btnCancelarHotel = new System.Windows.Forms.Button();
            this.btnGuardarHotel = new System.Windows.Forms.Button();
            this.btnNuevoHotel = new System.Windows.Forms.Button();
            this.dtg_Hotel = new System.Windows.Forms.DataGridView();
            this.txtDireccionHotel = new System.Windows.Forms.TextBox();
            this.txtNombreHotel = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_Hotel)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(617, 99);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 18);
            this.label4.TabIndex = 60;
            this.label4.Text = "Telefono del Hotel";
            // 
            // txtTelefonoHotel
            // 
            this.txtTelefonoHotel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTelefonoHotel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefonoHotel.Location = new System.Drawing.Point(571, 131);
            this.txtTelefonoHotel.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtTelefonoHotel.Name = "txtTelefonoHotel";
            this.txtTelefonoHotel.Size = new System.Drawing.Size(211, 26);
            this.txtTelefonoHotel.TabIndex = 59;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(323, 99);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 18);
            this.label3.TabIndex = 58;
            this.label3.Text = "Direccion del Hotel";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(40, 99);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 18);
            this.label2.TabIndex = 57;
            this.label2.Text = "Nombre del Hotel";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(360, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 18);
            this.label1.TabIndex = 56;
            this.label1.Text = "Buscar";
            // 
            // txtBuscarHotel
            // 
            this.txtBuscarHotel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBuscarHotel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscarHotel.Location = new System.Drawing.Point(6, 44);
            this.txtBuscarHotel.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtBuscarHotel.Name = "txtBuscarHotel";
            this.txtBuscarHotel.Size = new System.Drawing.Size(776, 26);
            this.txtBuscarHotel.TabIndex = 55;
            this.txtBuscarHotel.TextChanged += new System.EventHandler(this.txtBuscarHotel_TextChanged);
            // 
            // btnEliminarHotel
            // 
            this.btnEliminarHotel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarHotel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarHotel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarHotel.Location = new System.Drawing.Point(682, 345);
            this.btnEliminarHotel.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnEliminarHotel.Name = "btnEliminarHotel";
            this.btnEliminarHotel.Size = new System.Drawing.Size(100, 50);
            this.btnEliminarHotel.TabIndex = 54;
            this.btnEliminarHotel.Text = "Eliminar";
            this.btnEliminarHotel.UseVisualStyleBackColor = true;
            this.btnEliminarHotel.Click += new System.EventHandler(this.btnEliminarHotel_Click_1);
            // 
            // btnCancelarHotel
            // 
            this.btnCancelarHotel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarHotel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelarHotel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelarHotel.Location = new System.Drawing.Point(570, 345);
            this.btnCancelarHotel.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnCancelarHotel.Name = "btnCancelarHotel";
            this.btnCancelarHotel.Size = new System.Drawing.Size(100, 50);
            this.btnCancelarHotel.TabIndex = 53;
            this.btnCancelarHotel.Text = "Cancelar";
            this.btnCancelarHotel.UseVisualStyleBackColor = true;
            this.btnCancelarHotel.Click += new System.EventHandler(this.btnCancelarHotel_Click);
            // 
            // btnGuardarHotel
            // 
            this.btnGuardarHotel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarHotel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarHotel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarHotel.Location = new System.Drawing.Point(458, 345);
            this.btnGuardarHotel.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnGuardarHotel.Name = "btnGuardarHotel";
            this.btnGuardarHotel.Size = new System.Drawing.Size(100, 50);
            this.btnGuardarHotel.TabIndex = 52;
            this.btnGuardarHotel.Text = "Guardar";
            this.btnGuardarHotel.UseVisualStyleBackColor = true;
            this.btnGuardarHotel.Click += new System.EventHandler(this.btnGuardarHotel_Click);
            // 
            // btnNuevoHotel
            // 
            this.btnNuevoHotel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevoHotel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevoHotel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNuevoHotel.Location = new System.Drawing.Point(346, 345);
            this.btnNuevoHotel.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnNuevoHotel.Name = "btnNuevoHotel";
            this.btnNuevoHotel.Size = new System.Drawing.Size(100, 50);
            this.btnNuevoHotel.TabIndex = 51;
            this.btnNuevoHotel.Text = "Nuevo";
            this.btnNuevoHotel.UseVisualStyleBackColor = true;
            this.btnNuevoHotel.Click += new System.EventHandler(this.btnNuevoHotel_Click);
            // 
            // dtg_Hotel
            // 
            this.dtg_Hotel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg_Hotel.Location = new System.Drawing.Point(6, 184);
            this.dtg_Hotel.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.dtg_Hotel.Name = "dtg_Hotel";
            this.dtg_Hotel.Size = new System.Drawing.Size(776, 139);
            this.dtg_Hotel.TabIndex = 50;
            this.dtg_Hotel.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_Hotel_CellContentClick);
            // 
            // txtDireccionHotel
            // 
            this.txtDireccionHotel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDireccionHotel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDireccionHotel.Location = new System.Drawing.Point(288, 131);
            this.txtDireccionHotel.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtDireccionHotel.Name = "txtDireccionHotel";
            this.txtDireccionHotel.Size = new System.Drawing.Size(213, 26);
            this.txtDireccionHotel.TabIndex = 49;
            // 
            // txtNombreHotel
            // 
            this.txtNombreHotel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNombreHotel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreHotel.Location = new System.Drawing.Point(6, 131);
            this.txtNombreHotel.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtNombreHotel.Name = "txtNombreHotel";
            this.txtNombreHotel.Size = new System.Drawing.Size(213, 26);
            this.txtNombreHotel.TabIndex = 48;
            // 
            // Frm_Hotel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 413);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtTelefonoHotel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBuscarHotel);
            this.Controls.Add(this.btnEliminarHotel);
            this.Controls.Add(this.btnCancelarHotel);
            this.Controls.Add(this.btnGuardarHotel);
            this.Controls.Add(this.btnNuevoHotel);
            this.Controls.Add(this.dtg_Hotel);
            this.Controls.Add(this.txtDireccionHotel);
            this.Controls.Add(this.txtNombreHotel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Hotel";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hotel";
            this.Load += new System.EventHandler(this.Frm_Hotel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtg_Hotel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTelefonoHotel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscarHotel;
        private System.Windows.Forms.Button btnEliminarHotel;
        private System.Windows.Forms.Button btnCancelarHotel;
        private System.Windows.Forms.Button btnGuardarHotel;
        private System.Windows.Forms.Button btnNuevoHotel;
        private System.Windows.Forms.DataGridView dtg_Hotel;
        private System.Windows.Forms.TextBox txtDireccionHotel;
        private System.Windows.Forms.TextBox txtNombreHotel;
    }
}