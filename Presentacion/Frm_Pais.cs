﻿using Entidades;
using Manejadores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Frm_Pais : Form
    {
        private ManejadorPais _manejadorpais;
        private EntidadPais _entidadpais;
        private string banderaGuardarPais;
        public Frm_Pais()
        {
            InitializeComponent();
            _manejadorpais = new ManejadorPais();
            _entidadpais = new EntidadPais();
        }
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevoPais.Enabled = nuevo;
            btnGuardarPais.Enabled = guardar;
            btnCancelarPais.Enabled = cancelar;
            btnEliminarPais.Enabled = eliminar;
        }

        //Metodo para los textbox
        private void TextBox(Boolean nombrepais, Boolean numclubpais, Boolean representapais)
        {
            txtNombrePais.Enabled = nombrepais;
            txtNumClubsPais.Enabled = numclubpais;
            txtrepresentaPais.Enabled = representapais;
        }

        //Metodo para limpiar los textbox
        private void LimpiarTxt()
        {
            txtNombrePais.Clear();
            txtNumClubsPais.Clear();
            txtrepresentaPais.Clear();
        }
        //Metodo actualizar datagridview
        private void BuscarPais(string consulta)
        {
            dtg_Pais.DataSource = _manejadorpais.Mostrar(consulta);
        }

        //Metodo guardar
        private void GuardarPaises()
        {
            try
            {
                EntidadPais entidadpais = new EntidadPais();
                entidadpais.NombrePais = txtNombrePais.Text;
                entidadpais.NumClubs = int.Parse(txtNumClubsPais.Text);
                entidadpais.Representa = txtrepresentaPais.Text;

                var validar = _manejadorpais.ValidarPais(entidadpais);

                if (validar.Item1)
                {
                    _manejadorpais.PaisInsertar(entidadpais);
                    MessageBox.Show("El usuario se guardo correctamente");
                }

                else
                {
                    MessageBox.Show(validar.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("El campo clubs no puede ser vacio", "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Metodo actualizar
        private void ActualizarPaises()
        {
            _manejadorpais.PaisActualizar(new EntidadPais
            {
                IdPais = int.Parse(dtg_Pais.CurrentRow.Cells["idPais"].Value.ToString()),
                NombrePais = txtNombrePais.Text,
                NumClubs = int.Parse(txtNumClubsPais.Text),
                Representa = txtrepresentaPais.Text
            });
        }

        //Metodo eliminar
        private void EliminarPaises()
        {
            var idpaises = dtg_Pais.CurrentRow.Cells["IdPais"].Value.ToString();
            _manejadorpais.PaisEliminar(int.Parse(idpaises));
        }
        private void btnNuevoPais_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            TextBox(true, true, true);
            txtNombrePais.Focus();
            banderaGuardarPais = "guardar";
        }

        private void btnEliminarPais_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Quieres eliminar a el Pais:{_entidadpais.NombrePais}?", "Eliminar Pais", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                EliminarPaises();
                BuscarPais("");
                MessageBox.Show("Pais eliminado");
                Botonera(true, false, false, false);
                LimpiarTxt();
                TextBox(false, false, false);
            }
        }

        private void Frm_Pais_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            TextBox(false, false, false);
            BuscarPais("");
        }

        private void btnGuardarPais_Click(object sender, EventArgs e)
        {
            if (banderaGuardarPais == "guardar")
            {
                GuardarPaises();
                BuscarPais("");
            }
            else
            {
                ActualizarPaises();
                BuscarPais("");
            }
            Botonera(true, false, false, false);
            LimpiarTxt();
            TextBox(false, false, false);
        }

        private void btnCancelarPais_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            TextBox(false, false, false);
        }

        private void txtBuscarPais_TextChanged(object sender, EventArgs e)
        {
            BuscarPais(txtBuscarPais.Text);
        }

        private void dtg_Pais_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, true);
            TextBox(true, true, true);
            txtNombrePais.Focus();
            txtNumClubsPais.Text = dtg_Pais.CurrentRow.Cells["NumClubs"].Value.ToString();
            txtrepresentaPais.Text = dtg_Pais.CurrentRow.Cells["Representa"].Value.ToString();
            banderaGuardarPais = "actualizar";
        }
    }
}
