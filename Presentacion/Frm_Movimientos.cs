﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_Movimientos : Form
    {
        private ManejadorMovimientos _manejador;
        private EntidadMovimientos _entidad;
        private string banderaGuardar;
        public Frm_Movimientos()
        {
            InitializeComponent();
            _manejador = new ManejadorMovimientos();
            _entidad = new EntidadMovimientos();
        }
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }
        //Metodo para los textbox
        private void ControlTxt(Boolean idmovimiento, Boolean fkpartida, Boolean movimiento, Boolean comentario)
        {
            txtidmovimiento.Enabled = idmovimiento;
            txtmovimiento.Enabled = movimiento;
            txtcomentario.Enabled = comentario;
            txtfkidpartida.Enabled = fkpartida;


        }
        //Metodo para limpiar los textbox
        private void LimpiarTxt()
        {
            txtcomentario.Clear();
            txtfkidpartida.Clear();
            txtidmovimiento.Clear();
            txtmovimiento.Clear();

        }
        private void BuscarPartida(string consulta)
        {
            DtgMovimiento.DataSource = _manejador.Mostrar(consulta);
        }
        //Metodo guardar
        private void GuardarPartida()
        {
            EntidadMovimientos entidad = new EntidadMovimientos();
            entidad.movimiento = txtmovimiento.Text;
            entidad.fkpartida = int.Parse(txtfkidpartida.Text);
            entidad.comentario = txtcomentario.Text;


            var validar = _manejador.ValidarCampo(entidad);

            if (validar.Item1)
            {
                _manejador.Insertar(entidad);
                MessageBox.Show(" eljuego se guardo correctamente");
            }

            else
            {
                MessageBox.Show(validar.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //Metodo actualizar
        private void Actualizar()
        {
            _manejador.Actualizar(new EntidadMovimientos
            {

                idmovimiento = int.Parse(DtgMovimiento.CurrentRow.Cells["idmovimiento"].Value.ToString()),
                fkpartida = int.Parse(txtfkidpartida.Text),
                movimiento = txtmovimiento.Text,
                comentario = txtcomentario.Text,

            });
        }
        //Metodo eliminar
        private void Eliminar()
        {
            var idmovi = DtgMovimiento.CurrentRow.Cells["idmovimiento"].Value.ToString();
            _manejador.Eliminar(idmovi);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Quieres eliminar el movimiento{ _entidad.idmovimiento }?", "Eliminar Movimiento", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarPartida("");
                MessageBox.Show("Movimiento eliminado");
            }
        }

        private void Frm_Movimientos_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
            BuscarPartida(txtidmovimiento.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            ControlTxt(true, true, true, true);
            txtidmovimiento.Focus();
            banderaGuardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {
                GuardarPartida();
                BuscarPartida("");
            }

            else
            {
                Actualizar();
                BuscarPartida("");
            }

            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
        }

        private void txtidmovimiento_TextChanged(object sender, EventArgs e)
        {
            BuscarPartida(txtidmovimiento.Text);
        }

        private void DtgMovimiento_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, true);
            ControlTxt(true, true, true, true);
            txtmovimiento.Focus();
            txtmovimiento.Text = DtgMovimiento.CurrentRow.Cells["movimiento"].Value.ToString();
            txtfkidpartida.Text = DtgMovimiento.CurrentRow.Cells["fkpartida"].Value.ToString();
            txtcomentario.Text = DtgMovimiento.CurrentRow.Cells["comentario"].Value.ToString();

            banderaGuardar = "actualizar";
        }
    }
}
