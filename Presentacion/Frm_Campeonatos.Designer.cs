﻿namespace Presentacion
{
    partial class Frm_Campeonatos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.DtgJugador = new System.Windows.Forms.DataGridView();
            this.txtJugador = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTipo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtgArbitro = new System.Windows.Forms.DataGridView();
            this.txtBuscarA = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnEliminarA = new System.Windows.Forms.Button();
            this.txtNombreA = new System.Windows.Forms.TextBox();
            this.btnCancelarA = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnGuardarA = new System.Windows.Forms.Button();
            this.txtTipoA = new System.Windows.Forms.TextBox();
            this.btnNuevoA = new System.Windows.Forms.Button();
            this.txtArbitro = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgJugador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgArbitro)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(960, 437);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.DtgJugador);
            this.tabPage1.Controls.Add(this.txtBuscar);
            this.tabPage1.Controls.Add(this.lblUsuario);
            this.tabPage1.Controls.Add(this.btnEliminar);
            this.tabPage1.Controls.Add(this.txtNombre);
            this.tabPage1.Controls.Add(this.btnCancelar);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.btnGuardar);
            this.tabPage1.Controls.Add(this.txtTipo);
            this.tabPage1.Controls.Add(this.btnNuevo);
            this.tabPage1.Controls.Add(this.txtJugador);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 30);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(952, 403);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Jugador";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.btnGuardarA);
            this.tabPage2.Controls.Add(this.dtgArbitro);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.txtBuscarA);
            this.tabPage2.Controls.Add(this.txtArbitro);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.btnNuevoA);
            this.tabPage2.Controls.Add(this.btnEliminarA);
            this.tabPage2.Controls.Add(this.txtTipoA);
            this.tabPage2.Controls.Add(this.txtNombreA);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.btnCancelarA);
            this.tabPage2.Location = new System.Drawing.Point(4, 30);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(952, 403);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Arbitro";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(452, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 18);
            this.label1.TabIndex = 71;
            this.label1.Text = "Buscar";
            // 
            // txtBuscar
            // 
            this.txtBuscar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBuscar.Location = new System.Drawing.Point(56, 30);
            this.txtBuscar.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(849, 26);
            this.txtBuscar.TabIndex = 1;
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged_1);
            // 
            // btnEliminar
            // 
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(793, 341);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(112, 56);
            this.btnEliminar.TabIndex = 65;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click_1);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(667, 341);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(112, 56);
            this.btnCancelar.TabIndex = 64;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click_1);
            // 
            // btnGuardar
            // 
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(541, 341);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(112, 56);
            this.btnGuardar.TabIndex = 63;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click_1);
            // 
            // btnNuevo
            // 
            this.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNuevo.Location = new System.Drawing.Point(415, 341);
            this.btnNuevo.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(112, 56);
            this.btnNuevo.TabIndex = 62;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click_1);
            // 
            // DtgJugador
            // 
            this.DtgJugador.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgJugador.Location = new System.Drawing.Point(56, 132);
            this.DtgJugador.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.DtgJugador.Name = "DtgJugador";
            this.DtgJugador.ReadOnly = true;
            this.DtgJugador.Size = new System.Drawing.Size(849, 180);
            this.DtgJugador.TabIndex = 70;
            this.DtgJugador.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgJugador_CellContentClick);
            // 
            // txtJugador
            // 
            this.txtJugador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJugador.Location = new System.Drawing.Point(662, 85);
            this.txtJugador.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtJugador.Name = "txtJugador";
            this.txtJugador.Size = new System.Drawing.Size(243, 26);
            this.txtJugador.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(751, 62);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 18);
            this.label3.TabIndex = 66;
            this.label3.Text = "Jugador";
            // 
            // txtTipo
            // 
            this.txtTipo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTipo.Location = new System.Drawing.Point(359, 85);
            this.txtTipo.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtTipo.Name = "txtTipo";
            this.txtTipo.Size = new System.Drawing.Size(243, 26);
            this.txtTipo.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(461, 62);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 18);
            this.label2.TabIndex = 68;
            this.label2.Text = "Tipo";
            // 
            // txtNombre
            // 
            this.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNombre.Location = new System.Drawing.Point(56, 85);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(243, 26);
            this.txtNombre.TabIndex = 2;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(145, 62);
            this.lblUsuario.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(64, 18);
            this.lblUsuario.TabIndex = 69;
            this.lblUsuario.Text = "Nombre";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(452, 3);
            this.label4.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 18);
            this.label4.TabIndex = 84;
            this.label4.Text = "Buscar";
            // 
            // dtgArbitro
            // 
            this.dtgArbitro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgArbitro.Location = new System.Drawing.Point(56, 132);
            this.dtgArbitro.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.dtgArbitro.Name = "dtgArbitro";
            this.dtgArbitro.ReadOnly = true;
            this.dtgArbitro.Size = new System.Drawing.Size(849, 180);
            this.dtgArbitro.TabIndex = 83;
            this.dtgArbitro.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgArbitro_CellContentClick);
            // 
            // txtBuscarA
            // 
            this.txtBuscarA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBuscarA.Location = new System.Drawing.Point(56, 30);
            this.txtBuscarA.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtBuscarA.Name = "txtBuscarA";
            this.txtBuscarA.Size = new System.Drawing.Size(849, 26);
            this.txtBuscarA.TabIndex = 72;
            this.txtBuscarA.TextChanged += new System.EventHandler(this.txtBuscarA_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(145, 62);
            this.label5.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 18);
            this.label5.TabIndex = 82;
            this.label5.Text = "Nombre";
            // 
            // btnEliminarA
            // 
            this.btnEliminarA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarA.Location = new System.Drawing.Point(793, 341);
            this.btnEliminarA.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnEliminarA.Name = "btnEliminarA";
            this.btnEliminarA.Size = new System.Drawing.Size(112, 56);
            this.btnEliminarA.TabIndex = 79;
            this.btnEliminarA.Text = "Eliminar";
            this.btnEliminarA.UseVisualStyleBackColor = true;
            this.btnEliminarA.Click += new System.EventHandler(this.btnEliminarA_Click);
            // 
            // txtNombreA
            // 
            this.txtNombreA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNombreA.Location = new System.Drawing.Point(56, 85);
            this.txtNombreA.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtNombreA.Name = "txtNombreA";
            this.txtNombreA.Size = new System.Drawing.Size(243, 26);
            this.txtNombreA.TabIndex = 73;
            // 
            // btnCancelarA
            // 
            this.btnCancelarA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelarA.Location = new System.Drawing.Point(667, 341);
            this.btnCancelarA.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnCancelarA.Name = "btnCancelarA";
            this.btnCancelarA.Size = new System.Drawing.Size(112, 56);
            this.btnCancelarA.TabIndex = 78;
            this.btnCancelarA.Text = "Cancelar";
            this.btnCancelarA.UseVisualStyleBackColor = true;
            this.btnCancelarA.Click += new System.EventHandler(this.btnCancelarA_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(461, 62);
            this.label6.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 18);
            this.label6.TabIndex = 81;
            this.label6.Text = "Tipo";
            // 
            // btnGuardarA
            // 
            this.btnGuardarA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarA.Location = new System.Drawing.Point(541, 341);
            this.btnGuardarA.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnGuardarA.Name = "btnGuardarA";
            this.btnGuardarA.Size = new System.Drawing.Size(112, 56);
            this.btnGuardarA.TabIndex = 77;
            this.btnGuardarA.Text = "Guardar";
            this.btnGuardarA.UseVisualStyleBackColor = true;
            this.btnGuardarA.Click += new System.EventHandler(this.btnGuardarA_Click);
            // 
            // txtTipoA
            // 
            this.txtTipoA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTipoA.Location = new System.Drawing.Point(359, 85);
            this.txtTipoA.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtTipoA.Name = "txtTipoA";
            this.txtTipoA.Size = new System.Drawing.Size(243, 26);
            this.txtTipoA.TabIndex = 74;
            // 
            // btnNuevoA
            // 
            this.btnNuevoA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevoA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNuevoA.Location = new System.Drawing.Point(415, 341);
            this.btnNuevoA.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnNuevoA.Name = "btnNuevoA";
            this.btnNuevoA.Size = new System.Drawing.Size(112, 56);
            this.btnNuevoA.TabIndex = 76;
            this.btnNuevoA.Text = "Nuevo";
            this.btnNuevoA.UseVisualStyleBackColor = true;
            this.btnNuevoA.Click += new System.EventHandler(this.btnNuevoA_Click);
            // 
            // txtArbitro
            // 
            this.txtArbitro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtArbitro.Location = new System.Drawing.Point(662, 85);
            this.txtArbitro.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtArbitro.Name = "txtArbitro";
            this.txtArbitro.Size = new System.Drawing.Size(243, 26);
            this.txtArbitro.TabIndex = 75;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(751, 62);
            this.label7.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 18);
            this.label7.TabIndex = 80;
            this.label7.Text = "Arbitro";
            // 
            // Frm_Campeonatos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 461);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Campeonatos";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Campeonatos";
            this.Load += new System.EventHandler(this.Frm_Campeonatos_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgJugador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgArbitro)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DtgJugador;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox txtTipo;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.TextBox txtJugador;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnGuardarA;
        private System.Windows.Forms.DataGridView dtgArbitro;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBuscarA;
        private System.Windows.Forms.TextBox txtArbitro;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnNuevoA;
        private System.Windows.Forms.Button btnEliminarA;
        private System.Windows.Forms.TextBox txtTipoA;
        private System.Windows.Forms.TextBox txtNombreA;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCancelarA;
    }
}