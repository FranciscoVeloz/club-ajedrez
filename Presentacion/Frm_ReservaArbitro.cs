﻿using Entidades;
using Manejadores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Frm_ReservaArbitro : Form
    {
        private ManejadorReservaArbitro _manejadorreservaA;
        private EntidadReservaArbitro _entidadreservaA;
        private string banderaGuardarReservaArbitro;
        public Frm_ReservaArbitro()
        {
            InitializeComponent();
            _manejadorreservaA = new ManejadorReservaArbitro();
            _entidadreservaA = new EntidadReservaArbitro();
        }
        //Metodo para botonera
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevoReservaArbitro.Enabled = nuevo;
            btnGuardarReservaArbitro.Enabled = guardar;
            btnCancelarReservaArbitro.Enabled = cancelar;
            btnEliminarReservaArbitro.Enabled = eliminar;
        }

        //Metodo para los textbox
        private void ControlTxt(Boolean fechaentradaA, Boolean fechasalidaA, Boolean fkhotel, Boolean fkidarbitro)
        {
            txtFechaEntradaArbitro.Enabled = fechaentradaA;
            txtFechaSalidaArbitro.Enabled = fechasalidaA;
            txtFkHotel.Enabled = fkhotel;
            txtFkIdArbitro.Enabled = fkidarbitro;
        }

        //Metodo para limpiar los textbox
        private void LimpiarTxt()
        {
            txtFechaEntradaArbitro.Clear();
            txtFechaSalidaArbitro.Clear();
            txtFkHotel.Clear();
            txtFkIdArbitro.Clear();
        }

        //Metodo actualizar datagridview
        private void BuscarReservaArbitro(string consulta)
        {
            dtg_ReservaArbitro.DataSource = _manejadorreservaA.Mostrar(consulta);
        }

        //Metodo guardar
        private void GuardarReservaArbitro()
        {
            EntidadReservaArbitro entidadreservaA = new EntidadReservaArbitro();
            entidadreservaA.FechaEntradaArbitro = txtFechaEntradaArbitro.Text;
            entidadreservaA.FechaSalidaArbitro = txtFechaSalidaArbitro.Text;
            entidadreservaA.Fkhotel = int.Parse(txtFkHotel.Text);
            entidadreservaA.FkIdarbitro = int.Parse(txtFkIdArbitro.Text);

            var validar = _manejadorreservaA.ValidarReservaArbitro(entidadreservaA);

            if (validar.Item1)
            {
                _manejadorreservaA.ReservaArbitroInsertar(entidadreservaA);
                MessageBox.Show("El usuario se guardo correctamente");
            }

            else
            {
                MessageBox.Show(validar.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Metodo actualizar
        private void Actualizar()
        {
            _manejadorreservaA.ReservaArbitroActualizar(new EntidadReservaArbitro
            {
                IdReservaArbitro = int.Parse(dtg_ReservaArbitro.CurrentRow.Cells["idReservaArbitro"].Value.ToString()),
                FechaEntradaArbitro = txtFechaEntradaArbitro.Text,
                FechaSalidaArbitro = txtFechaSalidaArbitro.Text,
                Fkhotel = int.Parse(txtFkHotel.Text),
                FkIdarbitro = int.Parse(txtFkIdArbitro.Text)
            });
        }

        //Metodo eliminar
        private void Eliminar()
        {
            var idreservaarbitro = dtg_ReservaArbitro.CurrentRow.Cells["IdReservaArbitro"].Value.ToString();
            _manejadorreservaA.ReservaArbitroEliminar(int.Parse(idreservaarbitro));
        }

        private void btnNuevoReservaArbitro_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            ControlTxt(true, true, true, true);
            txtFechaEntradaArbitro.Focus();
            banderaGuardarReservaArbitro = "guardar";
        }

        private void btnGuardarReservaArbitro_Click(object sender, EventArgs e)
        {
            if (banderaGuardarReservaArbitro == "guardar")
            {
                GuardarReservaArbitro();
                BuscarReservaArbitro("");
            }

            else
            {
                Actualizar();
                BuscarReservaArbitro("");
            }

            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
        }

        private void btnCancelarReservaArbitro_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
        }

        private void btnEliminarReservaArbitro_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Quieres eliminar a la reserva de Arbitro: { _entidadreservaA.FechaEntradaArbitro }?", "Eliminar Reserva Arbitro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarReservaArbitro("");
                MessageBox.Show("Reserva Arbitro eliminada");
            }
        }

        private void Frm_ReservaArbitro_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
            BuscarReservaArbitro(txtBuscarReservaArbitro.Text);
        }

        private void dtg_ReservaArbitro_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, true);
            ControlTxt(true, true, true, true);
            txtFechaEntradaArbitro.Focus();
            txtFechaSalidaArbitro.Text = dtg_ReservaArbitro.CurrentRow.Cells["FechaSalidaArbitro"].Value.ToString();
            txtFkHotel.Text = dtg_ReservaArbitro.CurrentRow.Cells["FkHotel"].Value.ToString();
            txtFkIdArbitro.Text = dtg_ReservaArbitro.CurrentRow.Cells["FkIdarbitro"].Value.ToString();
            banderaGuardarReservaArbitro = "actualizar";
        }

        private void txtBuscarReservaArbitro_TextChanged(object sender, EventArgs e)
        {
            BuscarReservaArbitro(txtBuscarReservaArbitro.Text);
        }
    }
}
