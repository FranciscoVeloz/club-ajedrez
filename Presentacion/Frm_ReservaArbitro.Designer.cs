﻿
namespace Presentacion
{
    partial class Frm_ReservaArbitro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.txtFkIdArbitro = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFkHotel = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscarReservaArbitro = new System.Windows.Forms.TextBox();
            this.btnEliminarReservaArbitro = new System.Windows.Forms.Button();
            this.btnCancelarReservaArbitro = new System.Windows.Forms.Button();
            this.btnGuardarReservaArbitro = new System.Windows.Forms.Button();
            this.dtg_ReservaArbitro = new System.Windows.Forms.DataGridView();
            this.txtFechaSalidaArbitro = new System.Windows.Forms.TextBox();
            this.txtFechaEntradaArbitro = new System.Windows.Forms.TextBox();
            this.btnNuevoReservaArbitro = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_ReservaArbitro)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(672, 106);
            this.label5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 18);
            this.label5.TabIndex = 103;
            this.label5.Text = "Arbitro";
            // 
            // txtFkIdArbitro
            // 
            this.txtFkIdArbitro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFkIdArbitro.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFkIdArbitro.Location = new System.Drawing.Point(591, 145);
            this.txtFkIdArbitro.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtFkIdArbitro.Name = "txtFkIdArbitro";
            this.txtFkIdArbitro.Size = new System.Drawing.Size(242, 26);
            this.txtFkIdArbitro.TabIndex = 102;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(945, 106);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 18);
            this.label4.TabIndex = 101;
            this.label4.Text = "Hotel";
            // 
            // txtFkHotel
            // 
            this.txtFkHotel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFkHotel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFkHotel.Location = new System.Drawing.Point(871, 145);
            this.txtFkHotel.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtFkHotel.Name = "txtFkHotel";
            this.txtFkHotel.Size = new System.Drawing.Size(223, 26);
            this.txtFkHotel.TabIndex = 100;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(359, 106);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 18);
            this.label3.TabIndex = 99;
            this.label3.Text = "Fecha de Salida";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 106);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 18);
            this.label2.TabIndex = 98;
            this.label2.Text = "Fecha de Entrada";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(524, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 18);
            this.label1.TabIndex = 97;
            this.label1.Text = "Buscar";
            // 
            // txtBuscarReservaArbitro
            // 
            this.txtBuscarReservaArbitro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBuscarReservaArbitro.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscarReservaArbitro.Location = new System.Drawing.Point(9, 38);
            this.txtBuscarReservaArbitro.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtBuscarReservaArbitro.Name = "txtBuscarReservaArbitro";
            this.txtBuscarReservaArbitro.Size = new System.Drawing.Size(1085, 26);
            this.txtBuscarReservaArbitro.TabIndex = 96;
            this.txtBuscarReservaArbitro.TextChanged += new System.EventHandler(this.txtBuscarReservaArbitro_TextChanged);
            // 
            // btnEliminarReservaArbitro
            // 
            this.btnEliminarReservaArbitro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarReservaArbitro.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarReservaArbitro.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarReservaArbitro.Location = new System.Drawing.Point(961, 427);
            this.btnEliminarReservaArbitro.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnEliminarReservaArbitro.Name = "btnEliminarReservaArbitro";
            this.btnEliminarReservaArbitro.Size = new System.Drawing.Size(133, 62);
            this.btnEliminarReservaArbitro.TabIndex = 95;
            this.btnEliminarReservaArbitro.Text = "Eliminar";
            this.btnEliminarReservaArbitro.UseVisualStyleBackColor = true;
            this.btnEliminarReservaArbitro.Click += new System.EventHandler(this.btnEliminarReservaArbitro_Click);
            // 
            // btnCancelarReservaArbitro
            // 
            this.btnCancelarReservaArbitro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarReservaArbitro.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelarReservaArbitro.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelarReservaArbitro.Location = new System.Drawing.Point(812, 427);
            this.btnCancelarReservaArbitro.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnCancelarReservaArbitro.Name = "btnCancelarReservaArbitro";
            this.btnCancelarReservaArbitro.Size = new System.Drawing.Size(133, 62);
            this.btnCancelarReservaArbitro.TabIndex = 94;
            this.btnCancelarReservaArbitro.Text = "Cancelar";
            this.btnCancelarReservaArbitro.UseVisualStyleBackColor = true;
            this.btnCancelarReservaArbitro.Click += new System.EventHandler(this.btnCancelarReservaArbitro_Click);
            // 
            // btnGuardarReservaArbitro
            // 
            this.btnGuardarReservaArbitro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarReservaArbitro.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarReservaArbitro.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarReservaArbitro.Location = new System.Drawing.Point(663, 427);
            this.btnGuardarReservaArbitro.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnGuardarReservaArbitro.Name = "btnGuardarReservaArbitro";
            this.btnGuardarReservaArbitro.Size = new System.Drawing.Size(133, 62);
            this.btnGuardarReservaArbitro.TabIndex = 93;
            this.btnGuardarReservaArbitro.Text = "Guardar";
            this.btnGuardarReservaArbitro.UseVisualStyleBackColor = true;
            this.btnGuardarReservaArbitro.Click += new System.EventHandler(this.btnGuardarReservaArbitro_Click);
            // 
            // dtg_ReservaArbitro
            // 
            this.dtg_ReservaArbitro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg_ReservaArbitro.Location = new System.Drawing.Point(9, 213);
            this.dtg_ReservaArbitro.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.dtg_ReservaArbitro.Name = "dtg_ReservaArbitro";
            this.dtg_ReservaArbitro.Size = new System.Drawing.Size(1085, 182);
            this.dtg_ReservaArbitro.TabIndex = 91;
            this.dtg_ReservaArbitro.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_ReservaArbitro_CellContentClick);
            // 
            // txtFechaSalidaArbitro
            // 
            this.txtFechaSalidaArbitro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFechaSalidaArbitro.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaSalidaArbitro.Location = new System.Drawing.Point(304, 145);
            this.txtFechaSalidaArbitro.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtFechaSalidaArbitro.Name = "txtFechaSalidaArbitro";
            this.txtFechaSalidaArbitro.Size = new System.Drawing.Size(251, 26);
            this.txtFechaSalidaArbitro.TabIndex = 90;
            // 
            // txtFechaEntradaArbitro
            // 
            this.txtFechaEntradaArbitro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFechaEntradaArbitro.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaEntradaArbitro.Location = new System.Drawing.Point(9, 145);
            this.txtFechaEntradaArbitro.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtFechaEntradaArbitro.Name = "txtFechaEntradaArbitro";
            this.txtFechaEntradaArbitro.Size = new System.Drawing.Size(259, 26);
            this.txtFechaEntradaArbitro.TabIndex = 89;
            // 
            // btnNuevoReservaArbitro
            // 
            this.btnNuevoReservaArbitro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevoReservaArbitro.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevoReservaArbitro.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNuevoReservaArbitro.Location = new System.Drawing.Point(513, 427);
            this.btnNuevoReservaArbitro.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnNuevoReservaArbitro.Name = "btnNuevoReservaArbitro";
            this.btnNuevoReservaArbitro.Size = new System.Drawing.Size(133, 62);
            this.btnNuevoReservaArbitro.TabIndex = 104;
            this.btnNuevoReservaArbitro.Text = "Nuevo";
            this.btnNuevoReservaArbitro.UseVisualStyleBackColor = true;
            this.btnNuevoReservaArbitro.Click += new System.EventHandler(this.btnNuevoReservaArbitro_Click);
            // 
            // Frm_ReservaArbitro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 526);
            this.Controls.Add(this.btnNuevoReservaArbitro);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtFkIdArbitro);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtFkHotel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBuscarReservaArbitro);
            this.Controls.Add(this.btnEliminarReservaArbitro);
            this.Controls.Add(this.btnCancelarReservaArbitro);
            this.Controls.Add(this.btnGuardarReservaArbitro);
            this.Controls.Add(this.dtg_ReservaArbitro);
            this.Controls.Add(this.txtFechaSalidaArbitro);
            this.Controls.Add(this.txtFechaEntradaArbitro);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ReservaArbitro";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reserva Arbitro";
            this.Load += new System.EventHandler(this.Frm_ReservaArbitro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtg_ReservaArbitro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFkIdArbitro;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFkHotel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscarReservaArbitro;
        private System.Windows.Forms.Button btnEliminarReservaArbitro;
        private System.Windows.Forms.Button btnCancelarReservaArbitro;
        private System.Windows.Forms.Button btnGuardarReservaArbitro;
        private System.Windows.Forms.DataGridView dtg_ReservaArbitro;
        private System.Windows.Forms.TextBox txtFechaSalidaArbitro;
        private System.Windows.Forms.TextBox txtFechaEntradaArbitro;
        private System.Windows.Forms.Button btnNuevoReservaArbitro;
    }
}