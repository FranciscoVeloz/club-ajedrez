﻿namespace Presentacion
{
    partial class Frm_Movimientos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtidmovimiento = new System.Windows.Forms.TextBox();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.DtgMovimiento = new System.Windows.Forms.DataGridView();
            this.txtmovimiento = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtcomentario = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtfkidpartida = new System.Windows.Forms.TextBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DtgMovimiento)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(495, 1);
            this.label1.Margin = new System.Windows.Forms.Padding(13, 0, 13, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 20);
            this.label1.TabIndex = 82;
            this.label1.Text = "IdMovimiento";
            // 
            // txtidmovimiento
            // 
            this.txtidmovimiento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtidmovimiento.Location = new System.Drawing.Point(22, 41);
            this.txtidmovimiento.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.txtidmovimiento.Name = "txtidmovimiento";
            this.txtidmovimiento.Size = new System.Drawing.Size(1055, 22);
            this.txtidmovimiento.TabIndex = 70;
            this.txtidmovimiento.TextChanged += new System.EventHandler(this.txtidmovimiento_TextChanged);
            // 
            // btnEliminar
            // 
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(939, 505);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(139, 48);
            this.btnEliminar.TabIndex = 77;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(768, 505);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(144, 48);
            this.btnCancelar.TabIndex = 76;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(568, 510);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(173, 43);
            this.btnGuardar.TabIndex = 75;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNuevo.Location = new System.Drawing.Point(363, 512);
            this.btnNuevo.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(163, 38);
            this.btnNuevo.TabIndex = 74;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // DtgMovimiento
            // 
            this.DtgMovimiento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgMovimiento.Location = new System.Drawing.Point(-24, 214);
            this.DtgMovimiento.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.DtgMovimiento.Name = "DtgMovimiento";
            this.DtgMovimiento.Size = new System.Drawing.Size(1115, 273);
            this.DtgMovimiento.TabIndex = 81;
            this.DtgMovimiento.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgMovimiento_CellContentClick);
            // 
            // txtmovimiento
            // 
            this.txtmovimiento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmovimiento.Location = new System.Drawing.Point(345, 170);
            this.txtmovimiento.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.txtmovimiento.Name = "txtmovimiento";
            this.txtmovimiento.Size = new System.Drawing.Size(321, 22);
            this.txtmovimiento.TabIndex = 72;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(471, 135);
            this.label3.Margin = new System.Windows.Forms.Padding(13, 0, 13, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 20);
            this.label3.TabIndex = 78;
            this.label3.Text = "Movimiento";
            // 
            // txtcomentario
            // 
            this.txtcomentario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcomentario.Location = new System.Drawing.Point(693, 170);
            this.txtcomentario.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.txtcomentario.Name = "txtcomentario";
            this.txtcomentario.Size = new System.Drawing.Size(383, 22);
            this.txtcomentario.TabIndex = 73;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(827, 135);
            this.label2.Margin = new System.Windows.Forms.Padding(13, 0, 13, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 20);
            this.label2.TabIndex = 79;
            this.label2.Text = "Comentario";
            // 
            // txtfkidpartida
            // 
            this.txtfkidpartida.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfkidpartida.Location = new System.Drawing.Point(11, 170);
            this.txtfkidpartida.Margin = new System.Windows.Forms.Padding(13, 9, 13, 9);
            this.txtfkidpartida.Name = "txtfkidpartida";
            this.txtfkidpartida.Size = new System.Drawing.Size(307, 22);
            this.txtfkidpartida.TabIndex = 71;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.Location = new System.Drawing.Point(125, 135);
            this.lblUsuario.Margin = new System.Windows.Forms.Padding(13, 0, 13, 0);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(109, 20);
            this.lblUsuario.TabIndex = 80;
            this.lblUsuario.Text = "Fk_idpartida";
            // 
            // Frm_Movimientos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 562);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtidmovimiento);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.DtgMovimiento);
            this.Controls.Add(this.txtmovimiento);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtcomentario);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtfkidpartida);
            this.Controls.Add(this.lblUsuario);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Movimientos";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Movimientos";
            this.Load += new System.EventHandler(this.Frm_Movimientos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DtgMovimiento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidmovimiento;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.DataGridView DtgMovimiento;
        private System.Windows.Forms.TextBox txtmovimiento;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtcomentario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtfkidpartida;
        private System.Windows.Forms.Label lblUsuario;
    }
}