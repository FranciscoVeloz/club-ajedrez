﻿using System;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_Campeonatos : Form
    {
        private ManejadorCampeonatoJug _manejadorJug;
        private ManejadorCampeonatoArb _manejadorArb;
        private EntidadCameponatoJugador _entidadJug;
        private EntidadCameponatoArbitro _entidadArb;
        private string banderaGuardarJug;
        private string banderaGuardarArb;

        public Frm_Campeonatos()
        {
            InitializeComponent();
            _manejadorJug = new ManejadorCampeonatoJug();
            _entidadJug = new EntidadCameponatoJugador();
            _manejadorArb = new ManejadorCampeonatoArb();
            _entidadArb = new EntidadCameponatoArbitro();
        }

        //Metodo para botonera
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        //Metodo para los textbox
        private void ControlTxt(Boolean nombre, Boolean tipo, Boolean jugador)
        {
            txtNombre.Enabled = nombre;
            txtTipo.Enabled = tipo;
            txtJugador.Enabled = jugador;
        }

        //Metodo para limpiar los textbox
        private void LimpiarTxt()
        {
            txtNombre.Clear();
            txtJugador.Clear();
            txtTipo.Clear();
        }

        //Metodo actualizar datagridview
        private void BuscarJugador(string consulta)
        {
            DtgJugador.DataSource = _manejadorJug.Mostrar(consulta);
            DtgJugador.AutoResizeColumns();
        }

        //Metodo guardar
        private void GuardarJugador()
        {
            try
            {
                EntidadCameponatoJugador entidad = new EntidadCameponatoJugador();
                entidad.Nombre = txtNombre.Text;
                entidad.Tipo = txtTipo.Text;
                entidad.FkJugador = int.Parse(txtJugador.Text);

                var validar = _manejadorJug.ValidarCampo(entidad);

                if (validar.Item1)
                {
                    _manejadorJug.Insertar(entidad);
                    MessageBox.Show("El usuario se guardo correctamente");
                }

                else
                {
                    MessageBox.Show(validar.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("El campo jugador no puede ser vacio", "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Metodo actualizar
        private void Actualizar()
        {
            _manejadorJug.Actualizar(new EntidadCameponatoJugador
            {
                IdCampeonato = int.Parse(DtgJugador.CurrentRow.Cells["idCampeonato"].Value.ToString()),
                Nombre = txtNombre.Text,
                Tipo = txtTipo.Text,
                FkJugador = int.Parse(txtJugador.Text)
            });
        }

        //Metodo eliminar
        private void Eliminar()
        {
            var idJugador = DtgJugador.CurrentRow.Cells["idCampeonato"].Value.ToString();
            _manejadorJug.Eliminar(int.Parse(idJugador));
        }

        private void Frm_Campeonatos_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false);
            BuscarJugador("");
        }

        private void btnNuevo_Click_1(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            ControlTxt(true, true, true);
            txtNombre.Focus();
            banderaGuardarJug = "guardar";
        }

        private void btnGuardar_Click_1(object sender, EventArgs e)
        {
            if (banderaGuardarJug == "guardar")
            {
                GuardarJugador();
                BuscarJugador("");
            }

            else
            {
                Actualizar();
                BuscarJugador("");
            }

            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false);
        }

        private void btnCancelar_Click_1(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false);
        }

        private void DtgJugador_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, true);
            ControlTxt(true, true, true);
            txtNombre.Focus();
            txtNombre.Text = DtgJugador.CurrentRow.Cells["nombre"].Value.ToString();
            txtTipo.Text = DtgJugador.CurrentRow.Cells["tipo"].Value.ToString();
            txtJugador.Text = DtgJugador.CurrentRow.Cells["fkJugador"].Value.ToString();
            banderaGuardarJug = "actualizar";
        }

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro que quieres eliminar este dato?", "Eliminar Jugador", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Botonera(true, false, false, false);
                LimpiarTxt();
                ControlTxt(false, false, false);
                BuscarJugador("");
                MessageBox.Show("Usuario eliminado");
            }
        }

        private void txtBuscar_TextChanged_1(object sender, EventArgs e)
        {
            BuscarJugador(txtBuscar.Text);
        }


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////// ARBITRO //////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Metodo para botonera
        private void BotoneraA(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevoA.Enabled = nuevo;
            btnGuardarA.Enabled = guardar;
            btnCancelarA.Enabled = cancelar;
            btnEliminarA.Enabled = eliminar;
        }

        //Metodo para los textbox
        private void ControlTxtA(Boolean nombre, Boolean tipo, Boolean jugador)
        {
            txtNombreA.Enabled = nombre;
            txtTipoA.Enabled = tipo;
            txtArbitro.Enabled = jugador;
        }

        //Metodo para limpiar los textbox
        private void LimpiarTxtA()
        {
            txtNombreA.Clear();
            txtArbitro.Clear();
            txtTipoA.Clear();
        }

        //Metodo actualizar datagridview
        private void BuscarArbitro(string consulta)
        {
            dtgArbitro.DataSource = _manejadorArb.Mostrar(consulta);
            dtgArbitro.AutoResizeColumns();
        }

        //Metodo guardar
        private void GuardarArbitro()
        {
            try
            {
                EntidadCameponatoArbitro entidad = new EntidadCameponatoArbitro();
                entidad.Nombre = txtNombreA.Text;
                entidad.Tipo = txtTipoA.Text;
                entidad.FkArbitro = int.Parse(txtArbitro.Text);

                var validar = _manejadorArb.ValidarCampo(entidad);

                if (validar.Item1)
                {
                    _manejadorArb.Insertar(entidad);
                    MessageBox.Show("El usuario se guardo correctamente");
                }

                else
                {
                    MessageBox.Show(validar.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("El campo arbitro no puede ser vacio", "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Metodo actualizar
        private void ActualizarA()
        {
            _manejadorArb.Actualizar(new EntidadCameponatoArbitro
            {
                IdCampeonato = int.Parse(dtgArbitro.CurrentRow.Cells["idCampeonato"].Value.ToString()),
                Nombre = txtNombreA.Text,
                Tipo = txtTipoA.Text,
                FkArbitro = int.Parse(txtArbitro.Text)
            });
        }

        //Metodo eliminar
        private void EliminarA()
        {
            var idArbitro = dtgArbitro.CurrentRow.Cells["idCampeonato"].Value.ToString();
            _manejadorArb.Eliminar(int.Parse(idArbitro));
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BotoneraA(true, false, false, false);
            LimpiarTxtA();
            ControlTxtA(false, false, false);
            BuscarArbitro("");
        }

        private void btnNuevoA_Click(object sender, EventArgs e)
        {
            BotoneraA(false, true, true, false);
            ControlTxtA(true, true, true);
            txtNombreA.Focus();
            banderaGuardarArb = "guardar";
        }

        private void btnGuardarA_Click(object sender, EventArgs e)
        {
            if (banderaGuardarArb == "guardar")
            {
                GuardarArbitro();
                BuscarArbitro("");
            }

            else
            {
                ActualizarA();
                BuscarArbitro("");
            }

            BotoneraA(true, false, false, false);
            LimpiarTxtA();
            ControlTxtA(false, false, false);
        }

        private void btnCancelarA_Click(object sender, EventArgs e)
        {
            BotoneraA(true, false, false, false);
            LimpiarTxtA();
            ControlTxtA(false, false, false);
        }

        private void btnEliminarA_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro que quieres eliminar este dato?", "Eliminar Jugador", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                EliminarA();
                BotoneraA(true, false, false, false);
                LimpiarTxtA();
                ControlTxtA(false, false, false);
                BuscarArbitro("");
                MessageBox.Show("Usuario eliminado");
            }
        }

        private void txtBuscarA_TextChanged(object sender, EventArgs e)
        {
            BuscarArbitro(txtBuscarA.Text);
        }

        private void dtgArbitro_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            BotoneraA(false, true, true, true);
            ControlTxtA(true, true, true);
            txtNombreA.Focus();
            txtNombreA.Text = dtgArbitro.CurrentRow.Cells["nombre"].Value.ToString();
            txtTipoA.Text = dtgArbitro.CurrentRow.Cells["tipo"].Value.ToString();
            txtArbitro.Text = dtgArbitro.CurrentRow.Cells["fkArbitro"].Value.ToString();
            banderaGuardarArb = "actualizar";
        }
    }
}