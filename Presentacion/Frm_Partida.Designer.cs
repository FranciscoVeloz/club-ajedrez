﻿namespace Presentacion
{
    partial class Frm_Partida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtidpartida = new System.Windows.Forms.TextBox();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.DtgPartida = new System.Windows.Forms.DataGridView();
            this.txtjornada = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtentradas = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtfkarbitro = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtfksala = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DtgPartida)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(457, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 16);
            this.label1.TabIndex = 69;
            this.label1.Text = "IdPartida";
            // 
            // txtidpartida
            // 
            this.txtidpartida.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtidpartida.Location = new System.Drawing.Point(24, 34);
            this.txtidpartida.Margin = new System.Windows.Forms.Padding(15, 9, 15, 9);
            this.txtidpartida.Name = "txtidpartida";
            this.txtidpartida.Size = new System.Drawing.Size(936, 22);
            this.txtidpartida.TabIndex = 57;
            this.txtidpartida.TextChanged += new System.EventHandler(this.txtidpartida_TextChanged);
            // 
            // btnEliminar
            // 
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(791, 415);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(15, 9, 15, 9);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(156, 48);
            this.btnEliminar.TabIndex = 64;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(612, 415);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(15, 9, 15, 9);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(162, 48);
            this.btnCancelar.TabIndex = 63;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(400, 415);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(15, 9, 15, 9);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(195, 48);
            this.btnGuardar.TabIndex = 62;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNuevo.Location = new System.Drawing.Point(200, 415);
            this.btnNuevo.Margin = new System.Windows.Forms.Padding(15, 9, 15, 9);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(183, 48);
            this.btnNuevo.TabIndex = 61;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // DtgPartida
            // 
            this.DtgPartida.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgPartida.Location = new System.Drawing.Point(24, 237);
            this.DtgPartida.Margin = new System.Windows.Forms.Padding(15, 9, 15, 9);
            this.DtgPartida.Name = "DtgPartida";
            this.DtgPartida.Size = new System.Drawing.Size(936, 160);
            this.DtgPartida.TabIndex = 68;
            this.DtgPartida.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgPartida_CellContentClick);
            // 
            // txtjornada
            // 
            this.txtjornada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtjornada.Location = new System.Drawing.Point(120, 115);
            this.txtjornada.Margin = new System.Windows.Forms.Padding(15, 9, 15, 9);
            this.txtjornada.Name = "txtjornada";
            this.txtjornada.Size = new System.Drawing.Size(276, 22);
            this.txtjornada.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(226, 90);
            this.label3.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 16);
            this.label3.TabIndex = 65;
            this.label3.Text = "Jornada";
            // 
            // txtentradas
            // 
            this.txtentradas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtentradas.Location = new System.Drawing.Point(588, 115);
            this.txtentradas.Margin = new System.Windows.Forms.Padding(15, 9, 15, 9);
            this.txtentradas.Name = "txtentradas";
            this.txtentradas.Size = new System.Drawing.Size(276, 22);
            this.txtentradas.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(691, 90);
            this.label2.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 16);
            this.label2.TabIndex = 66;
            this.label2.Text = "Entradas";
            // 
            // txtfkarbitro
            // 
            this.txtfkarbitro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfkarbitro.Location = new System.Drawing.Point(120, 183);
            this.txtfkarbitro.Margin = new System.Windows.Forms.Padding(15, 9, 15, 9);
            this.txtfkarbitro.Name = "txtfkarbitro";
            this.txtfkarbitro.Size = new System.Drawing.Size(276, 22);
            this.txtfkarbitro.TabIndex = 70;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(223, 158);
            this.label4.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 16);
            this.label4.TabIndex = 71;
            this.label4.Text = "FkArbitro";
            // 
            // txtfksala
            // 
            this.txtfksala.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfksala.Location = new System.Drawing.Point(588, 183);
            this.txtfksala.Margin = new System.Windows.Forms.Padding(15, 9, 15, 9);
            this.txtfksala.Name = "txtfksala";
            this.txtfksala.Size = new System.Drawing.Size(276, 22);
            this.txtfksala.TabIndex = 72;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(698, 158);
            this.label5.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 16);
            this.label5.TabIndex = 73;
            this.label5.Text = "FkSala";
            // 
            // Frm_Partida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 470);
            this.Controls.Add(this.txtfksala);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtfkarbitro);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtidpartida);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.DtgPartida);
            this.Controls.Add(this.txtjornada);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtentradas);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Partida";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Partida";
            this.Load += new System.EventHandler(this.Frm_Partida_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DtgPartida)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidpartida;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.DataGridView DtgPartida;
        private System.Windows.Forms.TextBox txtjornada;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtentradas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtfkarbitro;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtfksala;
        private System.Windows.Forms.Label label5;
    }
}