﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_Partida : Form
    {
        private ManejadorPartida _manejador;
        private EntidadPartida _entidad;
        private string banderaGuardar;
        public Frm_Partida()
        {
            InitializeComponent();
            _manejador = new ManejadorPartida();
            _entidad = new EntidadPartida();
        }
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }
        //Metodo para los textbox
        private void ControlTxt(Boolean idpartida, Boolean fkhotel, Boolean fksala, Boolean fkarbitro, Boolean jornada, Boolean entradas)
        {
            txtidpartida.Enabled = idpartida;
            txtjornada.Enabled = jornada;
            txtentradas.Enabled = entradas;
            txtfkarbitro.Enabled = fkarbitro;
            txtfksala.Enabled = fkhotel;
            
        }
        //Metodo para limpiar los textbox
        private void LimpiarTxt()
        {
            txtidpartida.Clear();
            txtentradas.Clear();
            txtfkarbitro.Clear();
            txtfksala.Clear();
            txtjornada.Clear();
        }
        private void BuscarPartida(string consulta)
        {
            DtgPartida.DataSource = _manejador.Mostrar(consulta);
        }
        //Metodo guardar
        private void GuardarPartida()
        {
            EntidadPartida entidad = new EntidadPartida(); 
            entidad.Fkarbitro = int.Parse(txtfkarbitro.Text);
            entidad.Fksala = int.Parse(txtfksala.Text);
            entidad.jornada = txtjornada.Text;
            entidad.Entradas = int.Parse(txtentradas.Text);

            var validar = _manejador.ValidarCampo(entidad);

            if (validar.Item1)
            {
                _manejador.Insertar(entidad);
                MessageBox.Show("La partida se guardo correctamente");
            }

            else
            {
                MessageBox.Show(validar.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //Metodo actualizar
        private void Actualizar()
        {
            _manejador.Actualizar(new EntidadPartida
            {
                idpartida = int.Parse(DtgPartida.CurrentRow.Cells["idpartida"].Value.ToString()),
                Entradas = int.Parse(txtentradas.Text),
                jornada = txtjornada.Text,
                Fksala = int.Parse(txtfksala.Text),
                Fkarbitro = int.Parse(txtfkarbitro.Text)
            });
        }
        //Metodo eliminar
        private void Eliminar()
        {
            var idpartida = DtgPartida.CurrentRow.Cells["idpartida"].Value.ToString();
            _manejador.Eliminar(idpartida);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show($"Quieres eliminar a la partida{ _entidad.idpartida }?", "Eliminar Partida", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarPartida("");
                MessageBox.Show("Partida eliminada");
            }
        }

        private void Frm_Partida_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false, false, false);
            BuscarPartida(txtidpartida.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            ControlTxt(true, true, true, true, true, true);
            txtidpartida.Focus();
            banderaGuardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {
                GuardarPartida();
                BuscarPartida("");
            }

            else
            {
                Actualizar();
                BuscarPartida("");
            }

            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false, false ,false);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false, false, false);
        }

        private void txtidpartida_TextChanged(object sender, EventArgs e)
        {
            BuscarPartida(txtidpartida.Text);
        }

        private void DtgPartida_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, true);
            ControlTxt(true, true, true, true, true, true);
            txtidpartida.Focus();
            txtfkarbitro.Text = DtgPartida.CurrentRow.Cells["Fkarbitro"].Value.ToString();
            txtfksala.Text = DtgPartida.CurrentRow.Cells["Fksala"].Value.ToString();
            txtentradas.Text = DtgPartida.CurrentRow.Cells["Entradas"].Value.ToString();
            txtjornada.Text = DtgPartida.CurrentRow.Cells["jornada"].Value.ToString();
            banderaGuardar = "actualizar";
        }
    }
}
