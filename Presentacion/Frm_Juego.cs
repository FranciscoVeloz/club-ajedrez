﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_Juego : Form
    {
        private ManejadorJuego _manejador;
        private EntidadJuego _entidad;
        private string banderaGuardar;
        public Frm_Juego()
        {
            InitializeComponent();
            _manejador = new ManejadorJuego();
            _entidad = new EntidadJuego();
        }
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }
        //Metodo para los textbox
        private void ControlTxt(Boolean idjuego, Boolean fkpartida, Boolean fkjugador, Boolean color)
        {
            txtidjuego.Enabled = idjuego;
            txtfkjugador.Enabled = fkjugador;
            txtfkidpartida.Enabled = fkpartida;
            txtcolor.Enabled = color;


        }
        //Metodo para limpiar los textbox
        private void LimpiarTxt()
        {
            txtcolor.Clear();
            txtfkidpartida.Clear();
            txtfkjugador.Clear();
            txtidjuego.Clear();

        }
        private void BuscarPartida(string consulta)
        {
            DtgJuego.DataSource = _manejador.Mostrar(consulta);
        }
        //Metodo guardar
        private void GuardarPartida()
        {
            EntidadJuego entidad = new EntidadJuego();
            entidad.FkPartida = int.Parse(txtfkidpartida.Text);
            entidad.FkParticipante = int.Parse(txtfkjugador.Text);
            entidad.Color = txtcolor.Text;


            var validar = _manejador.ValidarCampo(entidad);

            if (validar.Item1)
            {
                _manejador.Insertar(entidad);
                MessageBox.Show(" eljuego se guardo correctamente");
            }

            else
            {
                MessageBox.Show(validar.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //Metodo actualizar
        private void Actualizar()
        {
            _manejador.Actualizar(new EntidadJuego
            {

                IdJuego = int.Parse(DtgJuego.CurrentRow.Cells["IdJuego"].Value.ToString()),
                FkParticipante = int.Parse(txtfkjugador.Text),
                FkPartida = int.Parse(txtfkidpartida.Text),
                Color = txtcolor.Text,

            });
        }
        //Metodo eliminar
        private void Eliminar()
        {
            var idjuego = DtgJuego.CurrentRow.Cells["IdJuego"].Value.ToString();
            _manejador.Eliminar(idjuego);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show($"Quieres eliminar el juego{ _entidad.IdJuego }?", "Eliminar Juego", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarPartida("");
                MessageBox.Show("Juego eliminado");
            }
        }

        private void Frm_Juego_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
            BuscarPartida(txtidjuego.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            ControlTxt(true, true, true, true);
            txtidjuego.Focus();
            banderaGuardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {
                GuardarPartida();
                BuscarPartida("");
            }

            else
            {
                Actualizar();
                BuscarPartida("");
            }

            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
        }

        private void txtidjuego_TextChanged(object sender, EventArgs e)
        {
            BuscarPartida(txtidjuego.Text);
        }

        private void DtgJuego_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, true);
            ControlTxt(true, true, true, true);
            txtfkidpartida.Focus();
            txtfkjugador.Text = DtgJuego.CurrentRow.Cells["FkParticipante"].Value.ToString();
            txtfkidpartida.Text = DtgJuego.CurrentRow.Cells["FkPartida"].Value.ToString();
            txtcolor.Text = DtgJuego.CurrentRow.Cells["color"].Value.ToString();

            banderaGuardar = "actualizar";
        }

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {
            Eliminar();
            MessageBox.Show("Dato eliminado");
            BuscarPartida("");
            Botonera(true, false, false, false);
            LimpiarTxt();
            ControlTxt(false, false, false, false);
        }
    }
}
