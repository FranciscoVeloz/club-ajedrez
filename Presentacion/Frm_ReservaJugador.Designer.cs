﻿
namespace Presentacion
{
    partial class Frm_ReservaJugador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.txtFkIdHotel = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscarReservaJugador = new System.Windows.Forms.TextBox();
            this.btnEliminarReservaJugador = new System.Windows.Forms.Button();
            this.btnCancelarReservaJugador = new System.Windows.Forms.Button();
            this.btnGuardarReservaJugador = new System.Windows.Forms.Button();
            this.btnNuevoReservaJugador = new System.Windows.Forms.Button();
            this.dtg_ReservaJugador = new System.Windows.Forms.DataGridView();
            this.txtFechaSalidaJugador = new System.Windows.Forms.TextBox();
            this.txtFechaEntradaJugdor = new System.Windows.Forms.TextBox();
            this.txtFkIdJugador = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_ReservaJugador)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(956, 110);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 18);
            this.label4.TabIndex = 86;
            this.label4.Text = "Hotel";
            // 
            // txtFkIdHotel
            // 
            this.txtFkIdHotel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFkIdHotel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFkIdHotel.Location = new System.Drawing.Point(881, 149);
            this.txtFkIdHotel.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtFkIdHotel.Name = "txtFkIdHotel";
            this.txtFkIdHotel.Size = new System.Drawing.Size(223, 26);
            this.txtFkIdHotel.TabIndex = 85;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(369, 110);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 18);
            this.label3.TabIndex = 84;
            this.label3.Text = "Fecha de Salida";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(40, 110);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 18);
            this.label2.TabIndex = 83;
            this.label2.Text = "Fecha de Entrada";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(535, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 18);
            this.label1.TabIndex = 82;
            this.label1.Text = "Buscar";
            // 
            // txtBuscarReservaJugador
            // 
            this.txtBuscarReservaJugador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBuscarReservaJugador.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscarReservaJugador.Location = new System.Drawing.Point(20, 42);
            this.txtBuscarReservaJugador.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtBuscarReservaJugador.Name = "txtBuscarReservaJugador";
            this.txtBuscarReservaJugador.Size = new System.Drawing.Size(1085, 26);
            this.txtBuscarReservaJugador.TabIndex = 81;
            this.txtBuscarReservaJugador.TextChanged += new System.EventHandler(this.txtBuscarReservaJugador_TextChanged);
            // 
            // btnEliminarReservaJugador
            // 
            this.btnEliminarReservaJugador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarReservaJugador.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarReservaJugador.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarReservaJugador.Location = new System.Drawing.Point(972, 431);
            this.btnEliminarReservaJugador.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnEliminarReservaJugador.Name = "btnEliminarReservaJugador";
            this.btnEliminarReservaJugador.Size = new System.Drawing.Size(133, 62);
            this.btnEliminarReservaJugador.TabIndex = 80;
            this.btnEliminarReservaJugador.Text = "Eliminar";
            this.btnEliminarReservaJugador.UseVisualStyleBackColor = true;
            this.btnEliminarReservaJugador.Click += new System.EventHandler(this.btnEliminarReservaJugador_Click);
            // 
            // btnCancelarReservaJugador
            // 
            this.btnCancelarReservaJugador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarReservaJugador.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelarReservaJugador.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelarReservaJugador.Location = new System.Drawing.Point(823, 431);
            this.btnCancelarReservaJugador.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnCancelarReservaJugador.Name = "btnCancelarReservaJugador";
            this.btnCancelarReservaJugador.Size = new System.Drawing.Size(133, 62);
            this.btnCancelarReservaJugador.TabIndex = 79;
            this.btnCancelarReservaJugador.Text = "Cancelar";
            this.btnCancelarReservaJugador.UseVisualStyleBackColor = true;
            this.btnCancelarReservaJugador.Click += new System.EventHandler(this.btnCancelarReservaJugador_Click);
            // 
            // btnGuardarReservaJugador
            // 
            this.btnGuardarReservaJugador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarReservaJugador.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarReservaJugador.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarReservaJugador.Location = new System.Drawing.Point(673, 431);
            this.btnGuardarReservaJugador.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnGuardarReservaJugador.Name = "btnGuardarReservaJugador";
            this.btnGuardarReservaJugador.Size = new System.Drawing.Size(133, 62);
            this.btnGuardarReservaJugador.TabIndex = 78;
            this.btnGuardarReservaJugador.Text = "Guardar";
            this.btnGuardarReservaJugador.UseVisualStyleBackColor = true;
            this.btnGuardarReservaJugador.Click += new System.EventHandler(this.btnGuardarReservaJugador_Click);
            // 
            // btnNuevoReservaJugador
            // 
            this.btnNuevoReservaJugador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevoReservaJugador.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevoReservaJugador.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNuevoReservaJugador.Location = new System.Drawing.Point(524, 431);
            this.btnNuevoReservaJugador.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnNuevoReservaJugador.Name = "btnNuevoReservaJugador";
            this.btnNuevoReservaJugador.Size = new System.Drawing.Size(133, 62);
            this.btnNuevoReservaJugador.TabIndex = 77;
            this.btnNuevoReservaJugador.Text = "Nuevo";
            this.btnNuevoReservaJugador.UseVisualStyleBackColor = true;
            this.btnNuevoReservaJugador.Click += new System.EventHandler(this.btnNuevoReservaJugador_Click);
            // 
            // dtg_ReservaJugador
            // 
            this.dtg_ReservaJugador.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg_ReservaJugador.Location = new System.Drawing.Point(20, 217);
            this.dtg_ReservaJugador.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.dtg_ReservaJugador.Name = "dtg_ReservaJugador";
            this.dtg_ReservaJugador.Size = new System.Drawing.Size(1085, 182);
            this.dtg_ReservaJugador.TabIndex = 76;
            this.dtg_ReservaJugador.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_ReservaJugador_CellContentClick);
            // 
            // txtFechaSalidaJugador
            // 
            this.txtFechaSalidaJugador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFechaSalidaJugador.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaSalidaJugador.Location = new System.Drawing.Point(315, 149);
            this.txtFechaSalidaJugador.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtFechaSalidaJugador.Name = "txtFechaSalidaJugador";
            this.txtFechaSalidaJugador.Size = new System.Drawing.Size(251, 26);
            this.txtFechaSalidaJugador.TabIndex = 75;
            // 
            // txtFechaEntradaJugdor
            // 
            this.txtFechaEntradaJugdor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFechaEntradaJugdor.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaEntradaJugdor.Location = new System.Drawing.Point(20, 149);
            this.txtFechaEntradaJugdor.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtFechaEntradaJugdor.Name = "txtFechaEntradaJugdor";
            this.txtFechaEntradaJugdor.Size = new System.Drawing.Size(259, 26);
            this.txtFechaEntradaJugdor.TabIndex = 74;
            // 
            // txtFkIdJugador
            // 
            this.txtFkIdJugador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFkIdJugador.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFkIdJugador.Location = new System.Drawing.Point(601, 149);
            this.txtFkIdJugador.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtFkIdJugador.Name = "txtFkIdJugador";
            this.txtFkIdJugador.Size = new System.Drawing.Size(242, 26);
            this.txtFkIdJugador.TabIndex = 87;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(683, 110);
            this.label5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 18);
            this.label5.TabIndex = 88;
            this.label5.Text = "Jugador";
            // 
            // Frm_ReservaJugador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1121, 514);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtFkIdJugador);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtFkIdHotel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBuscarReservaJugador);
            this.Controls.Add(this.btnEliminarReservaJugador);
            this.Controls.Add(this.btnCancelarReservaJugador);
            this.Controls.Add(this.btnGuardarReservaJugador);
            this.Controls.Add(this.btnNuevoReservaJugador);
            this.Controls.Add(this.dtg_ReservaJugador);
            this.Controls.Add(this.txtFechaSalidaJugador);
            this.Controls.Add(this.txtFechaEntradaJugdor);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ReservaJugador";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reserva Jugador";
            this.Load += new System.EventHandler(this.Frm_ReservaJugador_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtg_ReservaJugador)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFkIdHotel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscarReservaJugador;
        private System.Windows.Forms.Button btnEliminarReservaJugador;
        private System.Windows.Forms.Button btnCancelarReservaJugador;
        private System.Windows.Forms.Button btnGuardarReservaJugador;
        private System.Windows.Forms.Button btnNuevoReservaJugador;
        private System.Windows.Forms.DataGridView dtg_ReservaJugador;
        private System.Windows.Forms.TextBox txtFechaSalidaJugador;
        private System.Windows.Forms.TextBox txtFechaEntradaJugdor;
        private System.Windows.Forms.TextBox txtFkIdJugador;
        private System.Windows.Forms.Label label5;
    }
}