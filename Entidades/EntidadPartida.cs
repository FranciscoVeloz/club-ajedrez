﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class EntidadPartida
    {
        private int _IdPartida;
        private string _Jornada;
        private int _fkarbitro;
        private int _fksala;
        private int _entradas;

        public int idpartida { get => _IdPartida; set => _IdPartida = value; }
        public string jornada { get => _Jornada; set => _Jornada = value; }
        public int Fkarbitro { get => _fkarbitro; set => _fkarbitro = value; }
        public int Fksala { get => _fksala; set => _fksala = value; }
        public int Entradas { get => _entradas; set => _entradas = value; }
    }
}
