﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class EntidadReservaJugador
    {
        private int _IdReservaJugador;
        private string _FechaEntradaJugador;
        private string _FechaSalidaJugador;
        private int _FkIdhotel;
        private int _FkIdjugador;

        public int IdReservaJugador { get => _IdReservaJugador; set => _IdReservaJugador = value; }
        public string FechaEntradaJugador { get => _FechaEntradaJugador; set => _FechaEntradaJugador = value; }
        public string FechaSalidaJugador { get => _FechaSalidaJugador; set => _FechaSalidaJugador = value; }
        public int FkIdhotel { get => _FkIdhotel; set => _FkIdhotel = value; }
        public int FkIdjugador { get => _FkIdjugador; set => _FkIdjugador = value; }
    }
}
