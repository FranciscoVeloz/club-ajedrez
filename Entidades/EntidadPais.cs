﻿namespace Entidades
{
    public class EntidadPais
    {
        private int _IdPais;
        private string _NombrePais;
        private int _NumClubs;
        private string _Representa;

        public int IdPais { get => _IdPais; set => _IdPais = value; }
        public string NombrePais { get => _NombrePais; set => _NombrePais = value; }
        public int NumClubs { get => _NumClubs; set => _NumClubs = value; }
        public string Representa { get => _Representa; set => _Representa = value; }
    }
}