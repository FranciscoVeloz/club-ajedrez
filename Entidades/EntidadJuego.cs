﻿namespace Entidades
{
    public class EntidadJuego
    {
        private int _IdJuego;
        private int _FkPartida;
        private int _FkParticipante;
        private string _Color;

        public int IdJuego { get => _IdJuego; set => _IdJuego = value; }
        public int FkPartida { get => _FkPartida; set => _FkPartida = value; }
        public int FkParticipante { get => _FkParticipante; set => _FkParticipante = value; }
        public string Color { get => _Color; set => _Color = value; }
    }
}