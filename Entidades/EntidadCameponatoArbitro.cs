﻿namespace Entidades
{
    public class EntidadCameponatoArbitro
    {
        private int _IdCampeonato;
        private string _Nombre;
        private string _Tipo;
        private int _FkArbitro;

        public int IdCampeonato { get => _IdCampeonato; set => _IdCampeonato = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Tipo { get => _Tipo; set => _Tipo = value; }
        public int FkArbitro { get => _FkArbitro; set => _FkArbitro = value; }
    }
}
