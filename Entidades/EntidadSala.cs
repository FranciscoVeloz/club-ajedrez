﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class EntidadSala
    {
        
           
        private int idsala;
        private int fk_idhotel;
        private int capacidad;
        private string medios;

        public int Idsala { get => idsala; set => idsala = value; }
        public int Fk_idhotel { get => fk_idhotel; set => fk_idhotel = value; }
        public int Capacidad { get => capacidad; set => capacidad = value; }
        public string Medios { get => medios; set => medios = value; }
    
    }
}
