﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class EntidadHotel
    {
        private int _IdHotel;
        private string _NombreHotel;
        private string _DireccionHotel;
        private string _TelefonoHotel;

        public int IdHotel { get => _IdHotel; set => _IdHotel = value; }
        public string NombreHotel { get => _NombreHotel; set => _NombreHotel = value; }
        public string DireccionHotel { get => _DireccionHotel; set => _DireccionHotel = value; }
        public string TelefonoHotel { get => _TelefonoHotel; set => _TelefonoHotel = value; }
    }
}
