﻿namespace Entidades
{
    public class EntidadArbitro
    {
        private int _IdArbitro;
        private string _Nombre;
        private string _direccion;
        private int _FkPais;

        public int IdArbitro { get => _IdArbitro; set => _IdArbitro = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public int FkPais { get => _FkPais; set => _FkPais = value; }
    }
}