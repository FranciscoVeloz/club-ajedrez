﻿namespace Entidades
{
    public class EntidadJugador
    {
        private int _IdJugador;
        private string _Nombre;
        private string _Direccion;
        private int _FkPais;
        private int _Nivel;

        public int IdJugador { get => _IdJugador; set => _IdJugador = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Direccion { get => _Direccion; set => _Direccion = value; }
        public int FkPais { get => _FkPais; set => _FkPais = value; }
        public int Nivel { get => _Nivel; set => _Nivel = value; }
    }
}