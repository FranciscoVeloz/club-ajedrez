﻿namespace Entidades
{
    public class EntidadCameponatoJugador
    {
        private int _IdCampeonato;
        private string _Nombre;
        private string _Tipo;
        private int _FkJugador;

        public int IdCampeonato { get => _IdCampeonato; set => _IdCampeonato = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Tipo { get => _Tipo; set => _Tipo = value; }
        public int FkJugador { get => _FkJugador; set => _FkJugador = value; }
    }
}
