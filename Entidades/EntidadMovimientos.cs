﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class EntidadMovimientos
    {
        private int _IdMovimiento;
        private int _FkPartida;
        private string _Movimiento;
        private string _Comentario;

        public int idmovimiento { get => _IdMovimiento; set => _IdMovimiento = value; }
        public int fkpartida { get => _FkPartida; set => _FkPartida = value; }
        public string movimiento { get => _Movimiento; set => _Movimiento = value; }
        public string comentario { get => _Comentario; set => _Comentario = value; }
    }
}
