﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class EntidadReservaArbitro
    {
        private int _IdReservaArbitro;
        private string _FechaEntradaArbitro;
        private string _FechaSalidaArbitro;
        private int _FkIdhotel;
        private int _FkIdarbitro;

        public int IdReservaArbitro { get => _IdReservaArbitro; set => _IdReservaArbitro = value; }
        public string FechaEntradaArbitro { get => _FechaEntradaArbitro; set => _FechaEntradaArbitro = value; }
        public string FechaSalidaArbitro { get => _FechaSalidaArbitro; set => _FechaSalidaArbitro = value; }
        public int Fkhotel { get => _FkIdhotel; set => _FkIdhotel = value; }
        public int FkIdarbitro { get => _FkIdarbitro; set => _FkIdarbitro = value; }
    }
}
