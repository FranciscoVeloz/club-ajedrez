﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bases;
using Entidades;

namespace AccesoDatos
{
    public class PaisAccesoDatos
    {
        Conectar _conectar;

        public PaisAccesoDatos()
        {
            try
            {
                _conectar = new Conectar("localhost", "root", "", "clubajedrez");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarPais(EntidadPais entidadpais)
        {
            try
            {
                _conectar.Comando($"insert into Pais values(null,'{entidadpais.NombrePais}', '{entidadpais.NumClubs}', '{entidadpais.Representa}');");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarPais(EntidadPais entidadpais)
        {
            try
            {
                _conectar.Comando($"update Pais set nombre_Pais = '{entidadpais.NombrePais}' , num_Clubs = {entidadpais.NumClubs}, representa = '{entidadpais.Representa}' where id_Pais = {entidadpais.IdPais}");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void EliminarPais(string entidadpais)
        {
            try
            {
                _conectar.Comando($"delete from Pais where id_Pais = '{entidadpais}'");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<EntidadPais> ObtenerEntidadPais(string filtro)
        {

            var listaEntidadpais = new List<EntidadPais>();

            var ds = new DataSet();
            ds = _conectar.Consultar($"select * from Pais where nombre_Pais like '%{filtro}%'", "Pais");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var entidadpais = new EntidadPais
                {
                    IdPais = int.Parse(row["id_Pais"].ToString()),
                    NombrePais = row["nombre_Pais"].ToString(),
                    NumClubs = int.Parse(row["num_Clubs"].ToString()),
                    Representa = row["representa"].ToString()
                };

                listaEntidadpais.Add(entidadpais);
            }

            return listaEntidadpais;
        }
    }
}
