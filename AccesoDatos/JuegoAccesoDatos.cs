﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
using Bases;

namespace AccesoDatos
{
    public class JuegoAccesoDatos
    {
        Conectar _conectar;

        public JuegoAccesoDatos()
        {
            try
            {
                _conectar = new Conectar("localhost", "root", "", "clubajedrez");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarJuego(EntidadJuego entidadjuego)
        {
            try
            {
                _conectar.Comando($"insert into Juego values(null, {entidadjuego.FkPartida}, {entidadjuego.FkParticipante}, '{entidadjuego.Color}');");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarEntidadJuego(EntidadJuego entidadjuego)
        {
            try
            {
                _conectar.Comando($"update Juego set fk_idpartida = {entidadjuego.FkPartida}, fk_idjugador = {entidadjuego.FkParticipante}, " +
                    $"color = '{entidadjuego.Color}' where idjuego = {entidadjuego.IdJuego}");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void Eliminarjuego(string entidadjuego)
        {
            try
            {
                _conectar.Comando($"delete from Juego where idjuego = '{entidadjuego}'");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<EntidadJuego> ObtenerEntidadjuego(string filtro)
        {

            var listaEntidadjuego = new List<EntidadJuego>();

            var ds = new DataSet();
            ds = _conectar.Consultar($"select * from Juego where idjuego like '%{filtro}%'", "Juego");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var entidadjuego = new EntidadJuego
                {
                    IdJuego = int.Parse(row["idjuego"].ToString()),
                    FkPartida = int.Parse(row["fk_idpartida"].ToString()),
                    FkParticipante = int.Parse(row["fk_idjugador"].ToString()),
                    Color = row["color"].ToString()
                };

                listaEntidadjuego.Add(entidadjuego);
            }

            return listaEntidadjuego;
        }
    }
}
