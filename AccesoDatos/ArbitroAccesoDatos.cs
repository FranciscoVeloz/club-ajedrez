﻿using System;
using System.Collections.Generic;
using System.Data;
using Bases;
using Entidades;

namespace AccesoDatos
{
    public class ArbitroAccesoDatos
    {
        Conectar _conectar;

        public ArbitroAccesoDatos()
        {
            try
            {
                _conectar = new Conectar("localhost", "root", "", "clubajedrez");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarTemplate(EntidadArbitro enArb)
        {
            try
            {
                _conectar.Comando($"insert into Arbitro values(null, '{enArb.Nombre}', '{enArb.Direccion}', {enArb.FkPais});");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarTemplate(EntidadArbitro enArb)
        {
            try
            {
                _conectar.Comando($"update Arbitro set nombre = '{enArb.Nombre}', direccion = '{enArb.Direccion}', fkPais = {enArb.FkPais}  " +
                    $"where idArbitro = {enArb.IdArbitro}");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void EliminarTemplate(int entidad)
        {
            try
            {
                _conectar.Comando($"delete from Arbitro where idArbitro = {entidad}");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<EntidadArbitro> ObtenerEntidad(string filtro)
        {

            var listaEntidad = new List<EntidadArbitro>();

            var ds = new DataSet();
            ds = _conectar.Consultar($"select * from arbitro where nombre like '%{filtro}%'", "arbitro");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var entidad = new EntidadArbitro
                {
                    IdArbitro = int.Parse(row["idArbitro"].ToString()),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    FkPais = int.Parse(row["fkPais"].ToString())
                };

                listaEntidad.Add(entidad);
            }

            return listaEntidad;
        }
    }
}
