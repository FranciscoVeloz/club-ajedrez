﻿using System;
using System.Collections.Generic;
using System.Data;
using Bases;
using Entidades;

namespace AccesoDatos
{
    public class CampeonatoJugAccesoDatos
    {
        Conectar _conectar;

        public CampeonatoJugAccesoDatos()
        {
            try
            {
                _conectar = new Conectar("localhost", "root", "", "clubajedrez");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarTemplate(EntidadCameponatoJugador enCam)
        {
            try
            {
                _conectar.Comando($"insert into CampeonatoJugador values(null, '{enCam.Nombre}', '{enCam.Tipo}', {enCam.FkJugador});");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarTemplate(EntidadCameponatoJugador enCam)
        {
            try
            {
                _conectar.Comando($"update CampeonatoJugador set nombre = '{enCam.Nombre}', tipo = '{enCam.Tipo}', fkJugador = {enCam.FkJugador} " +
                    $"where id_campeonato = {enCam.IdCampeonato}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void EliminarTemplate(int entidad)
        {
            try
            {
                _conectar.Comando($"delete from CampeonatoJugador where id_campeonato = {entidad}");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<EntidadCameponatoJugador> ObtenerEntidad(string filtro)
        {

            var listaEntidad = new List<EntidadCameponatoJugador>();

            var ds = new DataSet();
            ds = _conectar.Consultar($"select * from CampeonatoJugador where nombre like '%{filtro}%'", "CampeonatoJugador");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var entidad = new EntidadCameponatoJugador
                {
                    IdCampeonato = int.Parse(row["id_campeonato"].ToString()),
                    Nombre = row["nombre"].ToString(),
                    Tipo = row["tipo"].ToString(),
                    FkJugador = int.Parse(row["fkJugador"].ToString())
                };

                listaEntidad.Add(entidad);
            }

            return listaEntidad;
        }
    }
}
