﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bases;
using Entidades;
using System.Data;

namespace AccesoDatos
{
    public class MovimientoAccesoDatos
    {
        Conectar _conectar;

        public MovimientoAccesoDatos()
        {
            try
            {
                _conectar = new Conectar("localhost", "root", "", "clubajedrez");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarMovimiento(EntidadMovimientos entidadMovimientos)
        {
            try
            {
                _conectar.Comando($"insert into movimientos values(null,'{entidadMovimientos.movimiento}', '{entidadMovimientos.comentario}', {entidadMovimientos.fkpartida});");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarEntidadMovimiento(EntidadMovimientos entidadMovimientos)
        {
            try
            {
                _conectar.Comando($"update movimientos set movimiento = '{entidadMovimientos.movimiento}', comentario = '{entidadMovimientos.comentario}'," +
                    $"fk_idpartida = {entidadMovimientos.fkpartida} where idmovimientos = {entidadMovimientos.idmovimiento}");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void EliminarMovimientos(string entidadmovimientos)
        {
            try
            {
                _conectar.Comando($"delete from movimientos where idmovimientos = '{entidadmovimientos}'");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<EntidadMovimientos> ObtenerEntidadMovimientos(string filtro)
        {

            var listaEntidadMovimientos = new List<EntidadMovimientos>();

            var ds = new DataSet();
            ds = _conectar.Consultar($"select * from movimientos where idmovimientos like '%{filtro}%'", "movimientos");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var entidadmovimientos = new EntidadMovimientos
                {
                    idmovimiento = int.Parse(row["idmovimientos"].ToString()),
                    movimiento = row["movimiento"].ToString(),
                    comentario = row["comentario"].ToString(),
                    fkpartida = int.Parse(row["fk_idpartida"].ToString())
                };

                listaEntidadMovimientos.Add(entidadmovimientos);
            }

            return listaEntidadMovimientos;
        }
    }
}
