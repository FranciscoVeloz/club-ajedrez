﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
using Bases;

namespace AccesoDatos
{
    public class PartidaAccesoDatos
    {
        Conectar _conectar;

        public PartidaAccesoDatos()
        {
            try
            {
                _conectar = new Conectar("localhost", "root", "", "clubajedrez");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarPartida(EntidadPartida entidadpartida)
        {
            try
            {
                _conectar.Comando($"insert into partida values(null,'{entidadpartida.jornada}', {entidadpartida.Entradas}, {entidadpartida.Fkarbitro}, {entidadpartida.Fksala});");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarEntidadPartida(EntidadPartida entidadpartida)
        {
            try
            {
                _conectar.Comando($"update partida set jornada = '{entidadpartida.jornada}', entradas = {entidadpartida.Entradas}, fk_idarbitro = {entidadpartida.Fkarbitro}," +
                    $" fk_idsala = {entidadpartida.Fksala} where idpartida = {entidadpartida.idpartida}");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void EliminarPartida(string entidadpartida)
        {
            try
            {
                _conectar.Comando($"delete from partida where idpartida = '{entidadpartida}'");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<EntidadPartida> ObtenerEntidadpartida(string filtro)
        {

            var listaEntidadpartida = new List<EntidadPartida>();

            var ds = new DataSet();
            ds = _conectar.Consultar($"select * from partida where idpartida like '%{filtro}%'", "partida");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var entidadpartida = new EntidadPartida
                {
                    idpartida = int.Parse(row["idpartida"].ToString()),
                    jornada = row["jornada"].ToString(),
                    Entradas = int.Parse(row["entradas"].ToString()),
                    Fkarbitro = int.Parse(row["fk_idarbitro"].ToString()),
                    Fksala = int.Parse(row["fk_idsala"].ToString())
                };

                listaEntidadpartida.Add(entidadpartida);
            }

            return listaEntidadpartida;
        }
    }
}
