﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bases;
using Entidades;
using System.Data;


namespace AccesoDatos
{
    public class SalaAccesoDatos
    {
        Conectar _conectar;

        public SalaAccesoDatos()
        {
            try
            {
                _conectar = new Conectar("localhost", "root", "", "clubajedrez");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarSala(EntidadSala entidadsala)
        {
            try
            {
                _conectar.Comando($"insert into sala values(null,'{entidadsala.Capacidad}', '{entidadsala.Medios}', '{entidadsala.Fk_idhotel}');");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarEntidadSala(EntidadSala entidadSala)
        {
            try
            {
                _conectar.Comando($"update sala set capacidad = {entidadSala.Capacidad}, medios = '{entidadSala.Medios}', " +
                    $" fkid_hotel = {entidadSala.Fk_idhotel} where idsala = {entidadSala.Idsala}");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void EliminarSala(string entidadsala)
        {
            try
            {
                _conectar.Comando($"delete from sala where idsala = '{entidadsala}'");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<EntidadSala> ObtenerEntidadsala(string filtro)
        {

            var listaEntidadsala = new List<EntidadSala>();

            var ds = new DataSet();
            ds = _conectar.Consultar($"select * from sala where idsala like '%{filtro}%'", "sala");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var entidadsala = new EntidadSala
                {
                    Idsala = int.Parse(row["idsala"].ToString()),
                    Capacidad = int.Parse(row["capacidad"].ToString()),
                    Medios = row["medios"].ToString(),
                    Fk_idhotel = int.Parse(row["fkid_hotel"].ToString())
                };

                listaEntidadsala.Add(entidadsala);
            }

            return listaEntidadsala;
        }
    }
}
