﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bases;
using Entidades;

namespace AccesoDatos
{
    public class ReservaJugadorAccesoDatos
    {
        Conectar _conectar;

        public ReservaJugadorAccesoDatos()
        {
            try
            {
                _conectar = new Conectar("localhost", "root", "", "clubajedrez");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarReservaJugador(EntidadReservaJugador entidadreservajugador)
        {
            try
            {
                _conectar.Comando($"insert into ReservaJugador values(null,'{entidadreservajugador.FechaEntradaJugador}', '{entidadreservajugador.FechaSalidaJugador}', '{entidadreservajugador.FkIdhotel}','{entidadreservajugador.FkIdjugador}');");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarReservaJugador(EntidadReservaJugador entidadreservajugador)
        {
            try
            {
                _conectar.Comando($"update ReservaJugador set fecha_entradJugador = '{entidadreservajugador.FechaEntradaJugador}', fecha_salidaJugador = '{entidadreservajugador.FechaSalidaJugador}', fk_idhotel = {entidadreservajugador.FkIdhotel}, fk_idjugador = {entidadreservajugador.FkIdjugador} where id_reservaJugador = {entidadreservajugador.IdReservaJugador}");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void EliminarReservaJugador(string entidadreservajugador)
        {
            try
            {
                _conectar.Comando($"delete from ReservaJugador where id_reservaJugador = '{entidadreservajugador}'");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<EntidadReservaJugador> ObtenerEntidadReservaJugador(string filtro)
        {
            var listaEntidadreservaJ = new List<EntidadReservaJugador>();

            var ds = new DataSet();
            ds = _conectar.Consultar($"select * from ReservaJugador where fecha_entradJugador like '%{filtro}%'", "ReservaJugador");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var entidadreservaJ = new EntidadReservaJugador
                {
                    IdReservaJugador = int.Parse(row["id_reservaJugador"].ToString()),
                    FechaEntradaJugador = row["fecha_entradJugador"].ToString(),
                    FechaSalidaJugador = row["fecha_salidaJugador"].ToString(),
                    FkIdhotel = int.Parse(row["fk_idhotel"].ToString()),
                    FkIdjugador = int.Parse(row["fk_idjugador"].ToString())
                };

                listaEntidadreservaJ.Add(entidadreservaJ);
            }

            return listaEntidadreservaJ;
        }
    }
}
