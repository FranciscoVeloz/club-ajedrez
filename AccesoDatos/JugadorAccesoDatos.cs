﻿using System;
using System.Collections.Generic;
using System.Data;
using Bases;
using Entidades;

namespace AccesoDatos
{
    public class JugadorAccesoDatos
    {
        Conectar _conectar;

        public JugadorAccesoDatos()
        {
            try
            {
                _conectar = new Conectar("localhost", "root", "", "clubajedrez");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarTemplate(EntidadJugador enJug)
        {
            try
            {
                _conectar.Comando($"insert into Jugador values(null, '{enJug.Nombre}', '{enJug.Direccion}', {enJug.FkPais}, {enJug.Nivel});");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarTemplate(EntidadJugador enJug)
        {
            try
            {
                _conectar.Comando($"update Jugador set nombre = '{enJug.Nombre}', direccion = '{enJug.Direccion}', fkPais = {enJug.FkPais}, " +
                    $"nivel = {enJug.Nivel} where idJugador = {enJug.IdJugador}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void EliminarTemplate(int entidad)
        {
            try
            {
                _conectar.Comando($"delete from Jugador where idJugador = {entidad}");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<EntidadJugador> ObtenerEntidad(string filtro)
        {

            var listaEntidad = new List<EntidadJugador>();

            var ds = new DataSet();
            ds = _conectar.Consultar($"select * from Jugador where nombre like '%{filtro}%'", "Jugador");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var entidad = new EntidadJugador
                {
                    IdJugador  = int.Parse(row["idJugador"].ToString()),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    FkPais = int.Parse(row["fkPais"].ToString()),
                    Nivel = int.Parse(row["nivel"].ToString())
                };

                listaEntidad.Add(entidad);
            }

            return listaEntidad;
        }
    }
}
