﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bases;
using Entidades;

namespace AccesoDatos
{
    public class ReservaArbitroAccesoDatos
    {
        Conectar _conectar;

        public ReservaArbitroAccesoDatos()
        {
            try
            {
                _conectar = new Conectar("localhost", "root", "", "clubajedrez");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarReservaArbitro(EntidadReservaArbitro entidadreservaarbitro)
        {
            try
            {
                _conectar.Comando($"insert into ReservaArbitro values(null,'{entidadreservaarbitro.FechaEntradaArbitro}', '{entidadreservaarbitro.FechaSalidaArbitro}', '{entidadreservaarbitro.Fkhotel}','{entidadreservaarbitro.FkIdarbitro}');");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarReservaArbitro(EntidadReservaArbitro entidadreservaarbitro)
        {
            try
            {
                _conectar.Comando($"update ReservaArbitro set fecha_entradaArbitro = '{entidadreservaarbitro.FechaEntradaArbitro}', fecha_salidaArbitro = '{entidadreservaarbitro.FechaSalidaArbitro}', fk_hotel = {entidadreservaarbitro.Fkhotel}, fk_idarbitro = {entidadreservaarbitro.FkIdarbitro} where id_reservaArbitro = {entidadreservaarbitro.IdReservaArbitro}");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void EliminarReservaArbitro(string entidadreservaarbitro)
        {
            try
            {
                _conectar.Comando($"delete from ReservaArbitro where id_reservaArbitro = '{entidadreservaarbitro}'");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<EntidadReservaArbitro> ObtenerEntidadReservaArbitro(string filtro)
        {
            var listaEntidadreservaA = new List<EntidadReservaArbitro>();

            var ds = new DataSet();
            ds = _conectar.Consultar($"select * from ReservaArbitro where fecha_entradaArbitro like '%{filtro}%'", "ReservaArbitro");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var entidadreservaA = new EntidadReservaArbitro
                {
                    IdReservaArbitro = int.Parse(row["id_reservaArbitro"].ToString()),
                    FechaEntradaArbitro = row["fecha_entradaArbitro"].ToString(),
                    FechaSalidaArbitro = row["fecha_salidaArbitro"].ToString(),
                    Fkhotel = int.Parse(row["fk_hotel"].ToString()),
                    FkIdarbitro = int.Parse(row["fk_idarbitro"].ToString())
                };

                listaEntidadreservaA.Add(entidadreservaA);
            }

            return listaEntidadreservaA;
        }
    }
}
