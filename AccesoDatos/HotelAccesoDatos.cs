﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bases;
using Entidades;

namespace AccesoDatos
{
    public class HotelAccesoDatos
    {
        Conectar _conectar;

        public HotelAccesoDatos()
        {
            try
            {
                _conectar = new Conectar("localhost", "root", "", "clubajedrez");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarHotel(EntidadHotel entidadhotel)
        {
            try
            {
                _conectar.Comando($"insert into Hotel values(null,'{entidadhotel.NombreHotel}', '{entidadhotel.DireccionHotel}', '{entidadhotel.TelefonoHotel}');");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarHotel(EntidadHotel entidadhotel)
        {
            try
            {
                _conectar.Comando($"update Hotel set nombre_hotel = '{entidadhotel.NombreHotel}', direccion_hotel = '{entidadhotel.DireccionHotel}', telefono_hotel = '{entidadhotel.TelefonoHotel}' where id_hotel = {entidadhotel.IdHotel}");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void EliminarHotel(string entidadhotel)
        {
            try
            {
                _conectar.Comando($"delete from Hotel where id_hotel = '{entidadhotel}'");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<EntidadHotel> ObtenerEntidadHotel(string filtro)
        {
            var listaEntidadhotel = new List<EntidadHotel>();

            var ds = new DataSet();
            ds = _conectar.Consultar($"select * from Hotel where nombre_hotel like '%{filtro}%'", "Hotel");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var entidadhotel = new EntidadHotel
                {
                    IdHotel = int.Parse(row["id_hotel"].ToString()),
                    NombreHotel = row["nombre_hotel"].ToString(),
                    DireccionHotel = row["direccion_hotel"].ToString(),
                    TelefonoHotel = row["telefono_hotel"].ToString()
                };

                listaEntidadhotel.Add(entidadhotel);
            }

            return listaEntidadhotel;
        }
    }
}
