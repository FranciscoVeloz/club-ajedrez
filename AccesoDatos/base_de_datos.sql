﻿create database clubajedrez;
use clubajedrez;

create table Pais(
	id_Pais int primary key auto_increment,
	nombre_Pais varchar(100),
	num_Clubs int,
	representa varchar(100)
);

create table Hotel(
	id_hotel int primary key auto_increment,
	nombre_hotel varchar(100),
	direccion_hotel varchar(100),
	telefono_hotel varchar(100)
);

create table Jugador(
	idJugador int primary key auto_increment,
	nombre varchar(100),
	direccion varchar(100),
	fkPais int,
	nivel int,
	foreign key(fkPais) references pais(id_Pais)
);

create table Arbitro(
	idArbitro int primary key auto_increment,
	nombre varchar(100),
	direccion varchar(100),
	fkPais int,
	foreign key(fkPais) references pais(id_Pais)
);

create table CampeonatoJugador(
	id_campeonato int primary key auto_increment,
	nombre varchar(100),
	tipo varchar(100),
	fkJugador int,
	foreign key(fkJugador) references Jugador(idJugador)
);

create table CampeonatoArbitro(
	id_campeonato int primary key auto_increment,
	nombre varchar(100),
	tipo varchar(100),
	fkArbitro int,
	foreign key(fkArbitro) references Arbitro(idArbitro)
);

create table ReservaJugador(
	id_reservaJugador int primary key auto_increment,
	fecha_entradJugador varchar(100),
	fecha_salidaJugador varchar(100),
	fk_idhotel int,
	fk_idjugador int,
	foreign key(fk_idhotel)references Hotel(id_hotel),
	foreign key(fk_idjugador)references Jugador(idJugador)
);

create table ReservaArbitro(
	id_reservaArbitro int primary key auto_increment,
	fecha_entradaArbitro varchar(100),
	fecha_salidaArbitro varchar(100),
	fk_hotel int,
	fk_idarbitro int,
	foreign key(fk_hotel)references Hotel(id_hotel),
	foreign key(fk_idarbitro)references Arbitro(idArbitro)
);

create table sala(
	idsala int primary key auto_increment,
	capacidad varchar(50),
	medios varchar(50),
	fkid_hotel int,
	foreign key(fkid_hotel)references Hotel(id_hotel)
);

create table partida(
	idpartida int primary key auto_increment,
	jornada varchar(50),
	entradas int,
	fk_idarbitro int,
	fk_idsala int,
	foreign key(fk_idarbitro)references Arbitro(idArbitro),
	foreign key(fk_idsala)references sala(idsala)
);

create table movimientos(
	 idmovimientos int primary key auto_increment,
	 movimiento varchar(50),
	 comentario varchar(50),
	 fk_idpartida int,
	 foreign key(fk_idpartida)references partida(idpartida)
 );

 create table Juego(
	 idjuego int primary key auto_increment,
	 fk_idpartida int,
	 fk_idjugador int,
	 color varchar(50),
	 foreign key(fk_idpartida)references partida(idpartida),
	 foreign key(fk_idjugador)references Jugador(idJugador)
 );

insert into Pais values
(null, "Mexico", 1, "Mx"),
(null, "Belice", 2, "Bel"),
(null, "Colombia", 3, "Col");

insert into Hotel values
(null, "Puesta del Sol", "Boulevar 23", "4491189906"),
(null, "Colombo", "Calle Lasaye 126", "4493458098"),
(null, "Quinta Cesar", "Malecon 55", "4492453310");

insert into Jugador values
(null, "Francisco", "Av. de los angeles", 1, 5),
(null, "Shaggy", "Lopez cotilla", 1, 8),
(null, "Rafael", "Las eibas", 1, 10);

insert into Arbitro values
(null, "Pedro", "Lomas del valle", 1),
(null, "Nestor", "Cañada", 1),
(null, "Juan", "Tepeyac", 1);

insert into CampeonatoJugador values
(null, "Liga Mx", "Estatal", 1),
(null, "La liga", "Nacional", 2),
(null, "Premier", "Internacional", 3);

insert into CampeonatoArbitro values
(null, "La liga", "Nacional", 1),
(null, "Premier", "Internacional", 3),
(null, "Liga Mx", "Estatal", 2);

insert into ReservaJugador values
(null, "06-11-2020", "12-11-2020",1,2),
(null, "09-11-2020", "18-11-2020",1,1),
(null, "10-11-2020", "20-11-2020",2,3);

insert into ReservaArbitro values
(null, "03-11-2020", "13-11-2020",1,1),
(null, "07-11-2020", "17-11-2020",1,2),
(null, "11-11-2020", "23-11-2020",2,3);

insert into sala values
(null, "1000", "TV",1),
(null, "2000", "RADIO Y TV",2),
(null, "855", "TV",3);

insert into partida values
(null, "A1", "654",1,2),
(null, "B3", "987",2,1),
(null, "C2", "468",3,3);

insert into movimientos values
(null, "Urracarrana", "se hace pasas desapersivido",3),
(null, "panenka", "juega con la mente del contrario",2),
(null, "chanfle", "hace trampa",1);

insert into Juego values
(null, 1, 1, "A1"),
(null, 3, 2, "B3"),
(null, 2, 3, "C2");