﻿using System;
using System.Collections.Generic;
using System.Data;
using Bases;
using Entidades;

namespace AccesoDatos
{
    public class CampeonatoArbAccesoDatos
    {
        Conectar _conectar;

        public CampeonatoArbAccesoDatos()
        {
            try
            {
                _conectar = new Conectar("localhost", "root", "", "clubajedrez");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarTemplate(EntidadCameponatoArbitro enCam)
        {
            try
            {
                _conectar.Comando($"insert into CampeonatoArbitro values(null, '{enCam.Nombre}', '{enCam.Tipo}', {enCam.FkArbitro});");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarTemplate(EntidadCameponatoArbitro enCam)
        {
            try
            {
                _conectar.Comando($"update CampeonatoArbitro set nombre = '{enCam.Nombre}', tipo = '{enCam.Tipo}', fkArbitro = {enCam.FkArbitro} " +
                    $"where id_campeonato = {enCam.IdCampeonato}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void EliminarTemplate(int entidad)
        {
            try
            {
                _conectar.Comando($"delete from CampeonatoArbitro where id_campeonato = {entidad}");

            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<EntidadCameponatoArbitro> ObtenerEntidad(string filtro)
        {

            var listaEntidad = new List<EntidadCameponatoArbitro>();

            var ds = new DataSet();
            ds = _conectar.Consultar($"select * from CampeonatoArbitro where nombre like '%{filtro}%'", "CampeonatoJugador");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var entidad = new EntidadCameponatoArbitro
                {
                    IdCampeonato = int.Parse(row["id_campeonato"].ToString()),
                    Nombre = row["nombre"].ToString(),
                    Tipo = row["tipo"].ToString(),
                    FkArbitro = int.Parse(row["fkArbitro"].ToString())
                };

                listaEntidad.Add(entidad);
            }

            return listaEntidad;
        }
    }
}
