﻿using AccesoDatos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manejadores
{
    public class ManejadorPais
    {
        PaisAccesoDatos _pais = new PaisAccesoDatos();

        ////Insertar datos
        public void PaisInsertar(EntidadPais entidadpais)
        {
            try
            {
                _pais.GuardarPais(entidadpais);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Eliminar datos
        public void PaisEliminar(int entidadpais)
        {
            try
            {
                _pais.EliminarPais(entidadpais.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Actualizar datos
        public void PaisActualizar(EntidadPais entidadpais)
        {
            try
            {
                _pais.ActualizarPais(entidadpais);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Mostrar datos
        public List<EntidadPais> Mostrar(string query)
        {
            var listaEntidadPais = _pais.ObtenerEntidadPais(query);
            return listaEntidadPais;
        }

        //Validar campos
        public Tuple<bool, string> ValidarPais(EntidadPais entidadpais)
        {
            bool error = true;
            string cadenaErrores = "";

            if (entidadpais.NombrePais.Length == 0 || entidadpais.NombrePais == null)
            {
                cadenaErrores += "* El campo nombre no puede ser vacio \n";
                error = false;
            }

            if (entidadpais.NumClubs == 0)
            {
                cadenaErrores += "* El campo clubs no puede ser vacio \n";
                error = false;
            }

            if (entidadpais.Representa.Length == 0)
            {
                cadenaErrores += "* El campo pais no puede ser vacio \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
    }
}
