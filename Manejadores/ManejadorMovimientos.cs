﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorMovimientos
    {
        MovimientoAccesoDatos _movi = new MovimientoAccesoDatos();

        ////Insertar datos
        public void Insertar(EntidadMovimientos entidad)
        {
            try
            {
                _movi.GuardarMovimiento(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Eliminar datos
        public void Eliminar(string entidad)
        {
            try
            {
                _movi.EliminarMovimientos(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Actualizar datos
        public void Actualizar(EntidadMovimientos entidad)
        {
            try
            {
                _movi.ActualizarEntidadMovimiento(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Mostrar datos
        public List<EntidadMovimientos> Mostrar(string query)
        {
            var listaEntidad = _movi.ObtenerEntidadMovimientos(query);
            return listaEntidad;
        }

        //Validar campos
        public Tuple<bool, string> ValidarCampo(EntidadMovimientos entidad)
        {
            bool error = true;
            string cadenaErrores = "";

            if (entidad.movimiento.Length == 0 || entidad.movimiento == null)
            {
                cadenaErrores += "* El campo movimiento no puede ser vacio \n";
                error = false;
            }
            if (entidad.comentario.Length == 0 || entidad.comentario == null)
            {
                cadenaErrores += "* El campo comentario no puede ser vacio \n";
                error = false;
            }
            if (entidad.fkpartida == 0)
            {
                cadenaErrores += "* El campo comentario no puede ser vacio \n";
                error = false;
            }
           

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
    }
}
