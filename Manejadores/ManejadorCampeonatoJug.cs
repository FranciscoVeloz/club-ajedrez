﻿using System;
using System.Collections.Generic;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorCampeonatoJug
    {
        CampeonatoJugAccesoDatos _jugador = new CampeonatoJugAccesoDatos();

        ////Insertar datos
        public void Insertar(EntidadCameponatoJugador entidad)
        {
            try
            {
                _jugador.GuardarTemplate(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Eliminar datos
        public void Eliminar(int entidad)
        {
            try
            {
                _jugador.EliminarTemplate(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Actualizar datos
        public void Actualizar(EntidadCameponatoJugador entidad)
        {
            try
            {
                _jugador.ActualizarTemplate(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Mostrar datos
        public List<EntidadCameponatoJugador> Mostrar(string query)
        {
            var listaEntidad = _jugador.ObtenerEntidad(query);
            return listaEntidad;
        }

        //Validar campos
        public Tuple<bool, string> ValidarCampo(EntidadCameponatoJugador entidad)
        {
            bool error = true;
            string cadenaErrores = "";

            if (entidad.Nombre.Length == 0 || entidad.Nombre == null)
            {
                cadenaErrores += "* El campo nombre no puede ser vacio \n";
                error = false;
            }

            if (entidad.Tipo.Length == 0 || entidad.Tipo == null)
            {
                cadenaErrores += "* El campo tipo no puede ser vacio \n";
                error = false;
            }

            if (entidad.FkJugador == 0)
            {
                cadenaErrores += "* El campo jugador no puede ser vacio \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
    }
}
