﻿using System;
using System.Collections.Generic;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorJugador
    {
        JugadorAccesoDatos _jugador = new JugadorAccesoDatos();

        ////Insertar datos
        public void Insertar(EntidadJugador entidad)
        {
            try
            {
                _jugador.GuardarTemplate(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Eliminar datos
        public void Eliminar(int entidad)
        {
            try
            {
                _jugador.EliminarTemplate(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Actualizar datos
        public void Actualizar(EntidadJugador entidad)
        {
            try
            {
                _jugador.ActualizarTemplate(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Mostrar datos
        public List<EntidadJugador> Mostrar(string query)
        {
            var listaEntidad = _jugador.ObtenerEntidad(query);
            return listaEntidad;
        }

        //Validar campos
        public Tuple<bool, string> ValidarCampo(EntidadJugador entidad)
        {
            bool error = true;
            string cadenaErrores = "";

            if (entidad.Nombre.Length == 0 || entidad.Nombre == null)
            {
                cadenaErrores += "* El campo nombre no puede ser vacio \n";
                error = false;
            }

            if (entidad.Direccion.Length == 0 || entidad.Direccion == null)
            {
                cadenaErrores += "* El campo direccion no puede ser vacio \n";
                error = false;
            }

            if (entidad.FkPais == 0)
            {
                cadenaErrores += "* El campo pais no puede ser vacio \n";
                error = false;
            }

            if (entidad.Nivel == 0)
            {
                cadenaErrores += "* El campo nivel no puede ser vacio \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
    }
}
