﻿using System;
using System.Collections.Generic;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorArbitro
    {
        ArbitroAccesoDatos _arbitro = new ArbitroAccesoDatos();

        ////Insertar datos
        public void Insertar(EntidadArbitro entidad)
        {
            try
            {
                _arbitro.GuardarTemplate(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Eliminar datos
        public void Eliminar(int entidad)
        {
            try
            {
                _arbitro.EliminarTemplate(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Actualizar datos
        public void Actualizar(EntidadArbitro entidad)
        {
            try
            {
                _arbitro.ActualizarTemplate(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Mostrar datos
        public List<EntidadArbitro> Mostrar(string query)
        {
            var listaEntidad = _arbitro.ObtenerEntidad(query);
            return listaEntidad;
        }

        //Validar campos
        public Tuple<bool, string> ValidarCampo(EntidadArbitro entidad)
        {
            bool error = true;
            string cadenaErrores = "";

            if (entidad.Nombre.Length == 0 || entidad.Nombre == null)
            {
                cadenaErrores += "* El campo nombre no puede ser vacio \n";
                error = false;
            }

            if (entidad.FkPais.ToString().Length == 0)
            {
                cadenaErrores += "* El campo pais no puede ser vacio \n";
                error = false;
            }

            if (entidad.Direccion.Length == 0 || entidad.Direccion == null)
            {
                cadenaErrores += "* El campo direccion no puede ser vacio \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
    }
}
