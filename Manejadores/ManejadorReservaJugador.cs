﻿using AccesoDatos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manejadores
{
    public class ManejadorReservaJugador
    {
        ReservaJugadorAccesoDatos _reservaJ = new ReservaJugadorAccesoDatos();

        ////Insertar datos
        public void ReservaJugadorInsertar(EntidadReservaJugador entidadreservajugador)
        {
            try
            {
                _reservaJ.GuardarReservaJugador(entidadreservajugador);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Eliminar datos
        public void ReservaJugadorEliminar(int entidadreservajugador)
        {
            try
            {
                _reservaJ.EliminarReservaJugador(entidadreservajugador.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Actualizar datos
        public void ReservaJugadorActualizar(EntidadReservaJugador entidadreservajugador)
        {
            try
            {
                _reservaJ.ActualizarReservaJugador(entidadreservajugador);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Mostrar datos
        public List<EntidadReservaJugador> Mostrar(string query)
        {
            var listaEntidadReservaJugador = _reservaJ.ObtenerEntidadReservaJugador(query);
            return listaEntidadReservaJugador;
        }

        //Validar campos
        public Tuple<bool, string> ValidarJugadorReserva(EntidadReservaJugador entidadreservajugador)
        {
            bool error = true;
            string cadenaErrores = "";

            if (entidadreservajugador.FechaEntradaJugador.Length == 0 || entidadreservajugador.FechaEntradaJugador == null)
            {
                cadenaErrores += "* El campo fecha entrada no puede ser vacio \n";
                error = false;
            }
            if (entidadreservajugador.FechaSalidaJugador.Length == 0 || entidadreservajugador.FechaSalidaJugador == null)
            {
                cadenaErrores += "* El campo fecha salida no puede ser vacio \n";
                error = false;
            }
            if (entidadreservajugador.FkIdhotel == 0)
            {
                cadenaErrores += "* El campo hotel no puede ser vacio \n";
                error = false;
            }
            if (entidadreservajugador.FkIdjugador == 0)
            {
                cadenaErrores += "* El campo jugador no puede ser vacio \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
    }
}
