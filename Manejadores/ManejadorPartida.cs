﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorPartida
    {
        PartidaAccesoDatos _partida = new PartidaAccesoDatos();

        ////Insertar datos
        public void Insertar(EntidadPartida entidad)
        {
            try
            {
                _partida.GuardarPartida(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Eliminar datos
        public void Eliminar(string entidad)
        {
            try
            {
                _partida.EliminarPartida(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Actualizar datos
        public void Actualizar(EntidadPartida entidad)
        {
            try
            {
                _partida.ActualizarEntidadPartida(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Mostrar datos
        public List<EntidadPartida> Mostrar(string query)
        {
            var listaEntidad = _partida.ObtenerEntidadpartida(query);
            return listaEntidad;
        }

        //Validar campos
        public Tuple<bool, string> ValidarCampo(EntidadPartida entidad)
        {
            bool error = true;
            string cadenaErrores = "";

            if (entidad.jornada.Length == 0 || entidad.jornada == null)
            {
                cadenaErrores += "* El campo jornada no puede ser vacio \n";
                error = false;
            }
            if (entidad.Entradas == 0)
            {
                cadenaErrores += "* El campo entrada no puede ser vacio \n";
                error = false;
            }
            
            if (entidad.Fkarbitro == 0)
            {
                cadenaErrores += "* El campo fkarbitro no puede ser vacio \n";
                error = false;
            }
            if (entidad.Fksala==0)
            {
                cadenaErrores += "* El campo fksala no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
    }
}
