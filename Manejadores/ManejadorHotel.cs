﻿using AccesoDatos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manejadores
{
    public class ManejadorHotel
    {
        HotelAccesoDatos _hotel = new HotelAccesoDatos();

        ////Insertar datos
        public void HotelInsertar(EntidadHotel entidadhotel)
        {
            try
            {
                _hotel.GuardarHotel(entidadhotel);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Eliminar datos
        public void HotelEliminar(int entidadhotel)
        {
            try
            {
                _hotel.EliminarHotel(entidadhotel.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Actualizar datos
        public void HotelActualizar(EntidadHotel entidadhotel)
        {
            try
            {
                _hotel.ActualizarHotel(entidadhotel);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Mostrar datos
        public List<EntidadHotel> Mostrar(string query)
        {
            var listaEntidadHotel = _hotel.ObtenerEntidadHotel(query);
            return listaEntidadHotel;
        }

        //Validar campos
        public Tuple<bool, string> ValidarHotel(EntidadHotel entidadhotel)
        {
            bool error = true;
            string cadenaErrores = "";

            if (entidadhotel.NombreHotel.Length == 0 || entidadhotel.NombreHotel == null)
            {
                cadenaErrores += "* El campo nombre no puede ser vacio \n";
                error = false;
            }

            if (entidadhotel.DireccionHotel.Length == 0 || entidadhotel.DireccionHotel == null)
            {
                cadenaErrores += "* El campo direccion no puede ser vacio \n";
                error = false;
            }

            if (entidadhotel.TelefonoHotel.Length == 0 || entidadhotel.TelefonoHotel == null)
            {
                cadenaErrores += "* El campo pais no puede ser vacio \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
    }
}
