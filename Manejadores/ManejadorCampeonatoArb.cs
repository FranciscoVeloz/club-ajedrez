﻿using System;
using System.Collections.Generic;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorCampeonatoArb
    {
        CampeonatoArbAccesoDatos _arbitro = new CampeonatoArbAccesoDatos();

        ////Insertar datos
        public void Insertar(EntidadCameponatoArbitro entidad)
        {
            try
            {
                _arbitro.GuardarTemplate(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Eliminar datos
        public void Eliminar(int entidad)
        {
            try
            {
                _arbitro.EliminarTemplate(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Actualizar datos
        public void Actualizar(EntidadCameponatoArbitro entidad)
        {
            try
            {
                _arbitro.ActualizarTemplate(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Mostrar datos
        public List<EntidadCameponatoArbitro> Mostrar(string query)
        {
            var listaEntidad = _arbitro.ObtenerEntidad(query);
            return listaEntidad;
        }

        //Validar campos
        public Tuple<bool, string> ValidarCampo(EntidadCameponatoArbitro entidad)
        {
            bool error = true;
            string cadenaErrores = "";

            if (entidad.Nombre.Length == 0 || entidad.Nombre == null)
            {
                cadenaErrores += "* El campo nombre no puede ser vacio \n";
                error = false;
            }

            if (entidad.Tipo.Length == 0 || entidad.Tipo == null)
            {
                cadenaErrores += "* El campo tipo no puede ser vacio \n";
                error = false;
            }

            if (entidad.FkArbitro == 0)
            {
                cadenaErrores += "* El campo arbitro no puede ser vacio \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
    }
}
