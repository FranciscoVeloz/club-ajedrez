﻿using AccesoDatos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manejadores
{
    public class ManejadorReservaArbitro
    {
        ReservaArbitroAccesoDatos _reservaA = new ReservaArbitroAccesoDatos();

        ////Insertar datos
        public void ReservaArbitroInsertar(EntidadReservaArbitro entidadreservaarbitro)
        {
            try
            {
                _reservaA.GuardarReservaArbitro(entidadreservaarbitro);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Eliminar datos
        public void ReservaArbitroEliminar(int entidadreservaarbitro)
        {
            try
            {
                _reservaA.EliminarReservaArbitro(entidadreservaarbitro.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Actualizar datos
        public void ReservaArbitroActualizar(EntidadReservaArbitro entidadreservaarbitro)
        {
            try
            {
                _reservaA.ActualizarReservaArbitro(entidadreservaarbitro);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Mostrar datos
        public List<EntidadReservaArbitro> Mostrar(string query)
        {
            var listaEntidadReservaArbitro = _reservaA.ObtenerEntidadReservaArbitro(query);
            return listaEntidadReservaArbitro;
        }

        //Validar campos
        public Tuple<bool, string> ValidarReservaArbitro(EntidadReservaArbitro entidadreservaarbitro)
        {
            bool error = true;
            string cadenaErrores = "";

            if (entidadreservaarbitro.FechaEntradaArbitro.Length == 0 || entidadreservaarbitro.FechaEntradaArbitro == null)
            {
                cadenaErrores += "* El campo fecha entrada no puede ser vacio \n";
                error = false;
            }
            if (entidadreservaarbitro.FechaSalidaArbitro.Length == 0 || entidadreservaarbitro.FechaSalidaArbitro == null)
            {
                cadenaErrores += "* El campo fecha salida no puede ser vacio \n";
                error = false;
            }
            if (entidadreservaarbitro.Fkhotel == 0)
            {
                cadenaErrores += "* El campo hotel no puede ser vacio \n";
                error = false;
            }
            if (entidadreservaarbitro.FkIdarbitro == 0)
            {
                cadenaErrores += "* El campo arbitro no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
    }
}
