﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorJuego
    {
        JuegoAccesoDatos _juego = new JuegoAccesoDatos();

        ////Insertar datos
        public void Insertar(EntidadJuego entidad)
        {
            try
            {
                _juego.GuardarJuego(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Eliminar datos
        public void Eliminar(string entidad)
        {
            try
            {
                _juego.Eliminarjuego(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Actualizar datos
        public void Actualizar(EntidadJuego entidad)
        {
            try
            {
                _juego.ActualizarEntidadJuego(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Mostrar datos
        public List<EntidadJuego> Mostrar(string query)
        {
            var listaEntidad = _juego.ObtenerEntidadjuego(query);
            return listaEntidad;
        }

        //Validar campos
        public Tuple<bool, string> ValidarCampo(EntidadJuego entidad)
        {
            bool error = true;
            string cadenaErrores = "";

            if (entidad.Color.Length == 0 || entidad.Color == null)
            {
                cadenaErrores += "* El campo juego no puede ser vacio \n";
                error = false;
            }
            if (entidad.FkParticipante == 0)
            {
                cadenaErrores += "* El campo fkparticipante no puede ser vacio \n";
                error = false;
            }
            if (entidad.FkPartida == 0)
            {
                cadenaErrores += "* El campo fkpartida no puede ser vacio \n";
                error = false;
            }
           

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
    }
}
