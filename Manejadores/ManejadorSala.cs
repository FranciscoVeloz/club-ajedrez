﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorSala
    {
        SalaAccesoDatos _sala = new SalaAccesoDatos();

        ////Insertar datos
        public void Insertar(EntidadSala entidad)
        {
            try
            {
                _sala.GuardarSala(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Eliminar datos
        public void Eliminar(string entidad)
        {
            try
            {
                _sala.EliminarSala(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Actualizar datos
        public void Actualizar(EntidadSala entidad)
        {
            try
            {
                _sala.ActualizarEntidadSala(entidad);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ////Mostrar datos
        public List<EntidadSala> Mostrar(string query)
        {
            var listaEntidad = _sala.ObtenerEntidadsala(query);
            return listaEntidad;
        }

        //Validar campos
        public Tuple<bool, string> ValidarCampo(EntidadSala entidad)
        {
            EntidadSala entidad1 = new EntidadSala();
            bool error = true;
            string cadenaErrores = "";

            if (entidad.Capacidad == 0)
            {
                cadenaErrores += "* El campo capacidad no puede ser vacio \n";
                error = false;
            }

            if (entidad.Medios.Length == 0 || entidad.Medios == null)
            {
                cadenaErrores += "* El campo medios no puede ser vacio \n";
                error = false;
            }

            if (entidad.Fk_idhotel == 0)
            {
                cadenaErrores += "* El campo fkhotel no puede ser vacio \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
    }
}
